from commons.services.user import UserService
from django.shortcuts import redirect
from django.conf import settings


def add_admin_check(view):
	def wrapper(request, slug=None, *args, **kwargs):
		userService = UserService(request.user)
		context = {'is_admin': userService.isAdmin()}
		args = args + (context,)
		if slug is not None:
			return view(request, slug, *args, **kwargs)

		return view(request, *args, **kwargs)
		
	return wrapper

def authenticated_redirect(view):
	def wrapper(request, *args, **kwargs):
		redirect_to = kwargs.get('next', settings.LOGIN_REDIRECT_URL )
		if request.user.is_authenticated:
			return redirect(redirect_to)

		return view(request, *args, **kwargs)

	return wrapper