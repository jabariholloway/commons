from django import template

register = template.Library()

@register.filter(name='initials')
def initials(value):
	initials = ''
	for word in value.split(' '):
		if len(word) > 0:
			initials = initials + word.upper()[0]
		

	return initials

