import commons.processes as proc
from commons.services.user import UserService
from django_cron import CronJobBase, Schedule
from commons.services.intervention import InterventionService
from datetime import datetime
import logging

logger = logging.getLogger(__name__)

#once per hour
def runScheduledTwitterInterventions():
	userService = UserService()
	sysUser = userService.getSystemUser()
	interventionService = InterventionService(sysUser)

	interventions = interventionService.getInterventionsByStatus('scheduled')

	currentTime = datetime.now

	for intervention in interventions:
		if currentTime >= intervention.start_time:
			interventionService.updateInterventionStatus(intervention.id, 'in progress')
			proc.runIntervention(intervention.id, sysUser.id)


#once per hour
#only for open conversations that are part of a running intervention
def pollActiveTwitterConversations():
	#there is a method for this already in processes 
	proc.updateActiveConversations()




class TestCronJob(CronJobBase):
    RUN_EVERY_MINS = 1 # every 2 hours

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'commons.test_cron_job'    # a unique code

    def do(self):
        print('test cron job')
        logger.info('test cron job') 


class RunTwitterInterventionsCronJob(CronJobBase):
	RUN_EVERY_MINS = 60 # every 1 hour

	schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
	code = 'commons.run_twitter_interventions_cron_job'    # a unique code

    def do(self):
        logger.info('running scheduled twitter interventions')
        runScheduledTwitterInterventions()

class PollTwitterConversationsCronJob(CronJobBase):
	RUN_EVERY_MINS = 60 # every 1 hour

	schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
	code = 'commons.poll_twitter_conversations_cron_job'    # a unique code

    def do(self):
        logger.info('polling active twitter conversations')
        pollActiveTwitterConversations()


class RunFacebookInterventionsCronJob(CronJobBase):
	RUN_EVERY_MINS = 60 # every 2 hours

	schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
	code = 'commons.run_facebook_interventions_cron_job'    # a unique code

    def do(self):
        logger.info('running scheduled facebook interventions')
        #all this does is check whether a facebook intervention
        #is in scheduled status, move it to in progress status,
        #and move interventions to complete/finished status

class PollFacebookConversationsCronJob(CronJobBase):
	RUN_EVERY_MINS = 60 # every 2 hours

	schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
	code = 'commons.poll_facebook_conversations_cron_job'    # a unique code

    def do(self):
        logger.info('polling active facebook conversations')
        #this works similar to polling active twitter conversations











