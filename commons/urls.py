"""commons URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from commons import views
from commons.forms import CustomRegistrationForm
from django.views.generic import TemplateView
from django_registration.views import RegistrationView
from commons.backends.accounts import RegistrationBackend
from django.contrib.auth import views as auth_views

urlpatterns = [
	path('', views.landing, name='landing'),
	path('projects/', views.projects, name='projects'),
	path('projects/create', views.createProject, name='create project'),
	path('projects/requests/create', views.createProjectRequest, name='create project request'),
	path('projects/details/<slug:slug>', views.projectDetails, name='project details'),
	path('projects/details/<slug:slug>/audiences/twitter', views.projectAudienceManagerTwitter, name='project audience manager'),
	path('projects/details/<slug:slug>/audiences/twitter/create', views.createTwitterAudience, name='create audience'),
	path('projects/details/<slug:slug>/audiences/twitter/download-hashtags', views.audienceHashtagsCsv, name='download-hashtags'),
	path('projects/details/<slug:slug>/interventions/twitter', views.projectInterventionsManagerTwitter, name='project twitter intervention manager'),
	path('projects/details/<slug:slug>/interventions/twitter/create', views.createTwitterIntervention, name='create twitter intervention'),
	path('projects/details/<slug:slug>/interventions/facebook', views.projectInterventionsManagerFacebook, name='project facebook intervention manager'),
	path('projects/details/<slug:slug>/interventions/facebook/create', views.createFacebookIntervention, name='create facebook intervention'),
	path('projects/details/<slug:slug>/conversations/twitter', views.projectFacilitatorConversationsManagerTwitter, name='project facilitator conversations manager'),
	path('projects/details/<slug:slug>/conversations/facebook', views.projectFacilitatorConversationsManagerFacebook, name='project facilitator conversations manager'),
	path('projects/details/<slug:slug>/dashboard', views.projectDashboard, name='project dashboard'),
	path('projects/details/<slug:slug>/overview', views.projectOverview, name='project overview'),
    path('admin/', admin.site.urls),
    path('oauth/', include('social_django.urls', namespace='social')),
    path('tasks/test', views.testBackgroundTask),
    #path('login/', views.login, name='login'),
    url(r'^users/!register', include('django_registration.backends.activation.urls')),
    url(r'^users/register/$', RegistrationBackend.as_view(), 
    	name='registration_register'),
    path('users/register/success', TemplateView.as_view(template_name='django_registration/registration_complete.html')),
    url(r'^users/profile/$', views.userProfile, name='user_profile'),
    path('logout/success', TemplateView.as_view(template_name='registration/logout.html')),
    url(r'^users/', include('django.contrib.auth.urls')),

    
    #path('register/', views.register, name='register'),
]
