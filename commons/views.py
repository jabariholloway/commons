from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from pprint import pprint
from social_django.models import UserSocialAuth
from commons.services.project import ProjectService 
from commons.services.user import UserService
from commons.services.audience import AudienceService
from commons.services.intervention import InterventionService
from django.http import JsonResponse
import dateparser
from commons.decorators import add_admin_check, authenticated_redirect
from django.core import serializers
import commons.tasks as tasks
import commons.processes as proc
import pytz
import logging
import csv
from django.http import HttpResponse

logger = logging.getLogger(__name__)

@login_required
def home(request):
	pprint(request.user.id)
	social_user = UserSocialAuth.objects.filter(user_id=request.user.id).first()
	pprint(social_user)
	return render(request, 'home.html', {'social_user': social_user})

@authenticated_redirect
def landing(request):
	return render(request, 'landing.html')

def login(request):
	return render(request, 'login.html')

def register(request):
	return render(request, 'register.html')

@login_required
def userProfile(request):
	userService = UserService(request.user)

	if request.method == 'POST':
		#update user profile
		firstName = request.POST.get('first_name')
		lastName = request.POST.get('last_name')
		isAvailable = request.POST.get('is_available')
		print('isAvailable: ' + str(isAvailable))
		dailyStartTime = request.POST.get('daily_start_time')
		dailyEndTime = request.POST.get('daily_end_time')
		print('dailyStartTime: ' + dailyStartTime)
		timeZone = request.POST.get('time_zone')

		userService.updateUserProfile(firstName, lastName, isAvailable, dailyStartTime, dailyEndTime, timeZone)
		profile = userService.getUserProfile()
		return render(request, 'profile.html', {'profile': profile, 'timezones': pytz.all_timezones, 'success_msg': 'Profile Updated'})
	else:
		profile = userService.getUserProfile()
		return render(request, 'profile.html', {'profile': profile, 'timezones': pytz.all_timezones})

@login_required
@add_admin_check
def projects(request, *args):
	projectService = ProjectService(request.user)
	projects = projectService.getAllProjects()
	projectMemberships = projectService.getProjectMemberships()
	userService = UserService(request.user)

	return render(request, 'projects.html', {'projects': projects, 'project_memberships': projectMemberships, **args[0]})

@login_required
def createProject(request):
	if request.method == 'POST':
		name = request.POST.get('project_name');
		short_description = request.POST.get('short_description')
		start_date = dateparser.parse(request.POST.get('start_date'))
		end_date = dateparser.parse(request.POST.get('end_date'))

		projectService = ProjectService(request.user)

		projectService.createProject(name, short_description, start_date, end_date)

		projects = projectService.getAllProjects()
		projectMemberships = projectService.getProjectMemberships()

		data = { 'projects' : list(projects.values()), 'projectMemberships' : list(projectMemberships.values()) }
		# for security, only return the newly created project

		return JsonResponse(data, safe=False)


@login_required
def createProjectRequest(request):
	if request.method == 'POST':
		slug = request.POST.get('project_slug')
		projectService = ProjectService(request.user)
		project = projectService.getProjectBySlug(slug)
		date = None

		if project.is_private:
			#create a default access request
			#that is pending approval
			#return the access request status
			#the front end will update
			#the project join button to "request pending"

			accessRequest = projectService.createProjectAccessRequest(project, 'requested')
			data = { 'project_url_slug': project.url_slug, 'project_request_status': accessRequest.request_status.name}
		else:
			#create a default access request
			#that is approved 
			#add the member to the project
			#return the access request status
			#and project information
			#the front end will update
			#the project join button to "joined"
			#and add the project to the user's
			#my projects list

			#lock down links(should only be able 
			#to view a project if you're an admin
			#or if you have access to the project
			accessRequest = projectService.createProjectAccessRequest(project, 'approved')
			projectService.addMember(request.user, project)
			data = { 'project_url_slug': project.url_slug, 'project_request_status': accessRequest.request_status.name, 'project_name': project.name, 'project_description': project.short_description}

		return JsonResponse(data, safe=False)




@login_required
@add_admin_check
def projectDetails(request, slug, *args):
	return render(request, 'project/details.html')

@login_required
@add_admin_check
def projectDashboard(request, slug, *args):
	projectService = ProjectService(request.user)
	return render(request, 'project/dashboard.html', { 'project': projectService.getProjectBySlug(slug), 'active_tab': 'dashboard', **args[0] })

@add_admin_check
def projectOverview(request, slug, *args):
	projectService = ProjectService(request.user)
	return render(request, 'project/my_overview.html', { 'project': projectService.getProjectBySlug(slug), 'active_tab': 'overview', **args[0] })

@login_required
@add_admin_check
def projectAudienceManagerTwitter(request, slug, *args):
	projectService = ProjectService(request.user)
	project = projectService.getProjectBySlug(slug)
	audienceService = AudienceService(request.user)
	audienceTypes = audienceService.getAudienceTypes()

	if request.method == 'GET':
		audiences = audienceService.getAudiences(project, 'twitter')
		return render(request, 'project/audience_manager_twitter.html', { 'project': projectService.getProjectBySlug(slug), 'audience_types': audienceTypes, 'audiences': audiences, 'active_tab': 'audiences_twitter', **args[0] })
	elif request.method == 'POST':
		name = request.POST.get('audience_name')
		description = request.POST.get('audience_description')
		audience_type_id = request.POST.get('audience_type')
		audienceType = audienceService.getAudienceType(audience_type_id)
		audiences = audienceService.getAudiences(project, 'twitter')
		audience = None

		if audienceType.name == 'seed' :
			csv_file = request.FILES['csv']
			new_file_path = handle_uploaded_audience_file(csv_file)
			with open(new_file_path) as f:
				try:
					audience = audienceService.createAudienceFromCsv(f, project, audienceName=name, audienceDescription=description)
					#kick off the seed handles hashtags background process
									#kick off background jobs
					logger.info('kicking off createSeedHashtags and createCoOccurringHashtags and createBasePopulation background process for project - ' + project.name + ' , audience - ' + audience.name)
					tasks.createSeedHashtags(audience.id, request.user.id)
					#logger.info('kicking off createBasePopulation background process for project - ' + project.name + ' , audience - ' + audience.name)
					#proc.createBasePopulation(audience.id, request.user.id)

					audiences = audienceService.getAudiences(project, 'twitter')
					return render(request, 'project/audience_manager_twitter.html', { 'project': projectService.getProjectBySlug(slug), 'audience_types': audienceTypes, 'audiences': audiences, 'new_audience': audience, 'show_next_steps_modal': True, 'active_tab': 'audiences_twitter', **args[0] })
				except ValueError as e:
					print(e)
					logger.error(e)
					return render(request, 'project/audience_manager_twitter.html', { 'project': projectService.getProjectBySlug(slug), 'audience_types': audienceTypes, 'audiences': audiences, 'new_audience': audience, 'show_next_steps_modal': True, 'error': e, 'active_tab': 'audiences_twitter', **args[0] })
		elif audienceType.name == 'base population':

			audienceMembersStr = request.POST.get('base_population')
			audience = audienceService.createAudienceFromString(audienceMembersStr, project, name, description, audienceTypeName='base population', statusName='ready')
			return render(request, 'project/audience_manager_twitter.html', { 'project': projectService.getProjectBySlug(slug), 'audience_types': audienceTypes, 'audiences': audiences, 'new_audience': audience, 'show_next_steps_modal': True,  'active_tab': 'audiences_twitter', **args[0] })

@add_admin_check
@login_required
def audienceHashtagsCsv(request, slug, *args):
	#call the audience service to get the csv rows
	#then write the rows here
	seedAudienceId =  int(request.GET.get('id', -1))
	if seedAudienceId > 0:
		audienceService = AudienceService(request.user)
		audience = audienceService.getAudience(seedAudienceId)
		csvData = audienceService.getSeedHashtagsCsvData(audience)

		response = HttpResponse(content_type='text/csv')
		response['Content-Disposition'] = 'attachment; filename="' + audience.name +  '_seed_and_co_occurring_hashtags.csv"'

		writer = csv.writer(response)

		for row in csvData:
			writer.writerow(row)

		return response



@login_required
@add_admin_check
def projectInterventionsManagerTwitter(request, slug, *args):
	projectService = ProjectService(request.user)
	project = projectService.getProjectBySlug(slug)
	interventionService = InterventionService(request.user)
	interventions = interventionService.getInterventions(project, 'twitter', 'created_date')
	audienceService = AudienceService(request.user)

	if request.method == 'GET':
		audiences = audienceService.getAudiences(project, 'twitter', audienceTypeName='base population')
		return render(request, 'project/interventions_manager_twitter.html', { 'project': projectService.getProjectBySlug(slug), 'interventions': interventions, 'audiences': audiences, 'active_tab': 'interventions_twitter', **args[0] })
	elif request.method == 'POST':
		name = request.POST.get('intervention_name');
		description = request.POST.get('intervention_description')
		hypothesis = request.POST.get('intervention_hypothesis')
		audience_id = request.POST.get('audience_id')
		start_date = request.POST.get('start_date')
		end_date = request.POST.get('end_date')
		csv_file = request.FILES['csv']


		audience = audienceService.getAudience(audience_id)
		intervention = interventionService.createIntervention(project,'twitter', audience, description, start_date, end_date, hypothesis=hypothesis)

		new_file_path = handle_uploaded_intervention_file(csv_file)
		with open(new_file_path, encoding='utf-8-sig') as f:
			try:
				interventionService.createInterventionHashtagsFromCsv(f, intervention)

				interventions = interventionService.getInterventions(project, 'twitter', 'created_date')

				audiences = audienceService.getAudiences(project, 'twitter')

				return render(request, 'project/interventions_manager_twitter.html', { 'project': projectService.getProjectBySlug(slug), 'audiences': audiences, 'interventions': interventions, 'new_audience': audience, 'show_next_steps_modal': True, 'active_tab': 'audiences_twitter', **args[0] })
			except ValueError as e:
				print(e)
				logger.error(e)
				return render(request, 'project/interventions_manager_twitter.html', { 'project': projectService.getProjectBySlug(slug), 'audiences': audiences, 'interventions': interventions, 'new_audience': audience, 'show_next_steps_modal': True, 'error': e, 'active_tab': 'audiences_twitter', **args[0] })

@login_required
@add_admin_check
def projectInterventionsManagerFacebook(request, slug, *args):
	projectService = ProjectService(request.user)
	project = projectService.getProjectBySlug(slug)
	interventionService = InterventionService(request.user)
	interventions = interventionService.getInterventions(project, 'twitter', '-created_date')

	if request.method == 'GET':
		return render(request, 'project/interventions_manager_facebook.html', { 'project': projectService.getProjectBySlug(slug), 'interventions': interventions, 'active_tab': 'interventions_twitter', **args[0] })

@login_required
def projectFacilitatorConversationsManagerTwitter(request, slug):
	projectService = ProjectService(request.user)
	project = projectService.getProjectBySlug(slug)
	interventionService = InterventionService(request.user)
	interventions = interventionService.getInterventions(project, 'twitter', '-created_date')
	mostRecentIntervention = None

	if (interventions is not None and len(interventions) > 0):
		mostRecentIntervention = interventions[0]

	if request.method == 'GET':
		return render(request, 'project/conversations_manager_twitter.html', { 'project': projectService.getProjectBySlug(slug), 'interventions': interventions, 'mostRecentIntervention': mostRecentIntervention, 'active_tab': 'conversations_twitter' })


@login_required
def projectFacilitatorConversationsManagerFacebook(request, slug):
	projectService = ProjectService(request.user)
	project = projectService.getProjectBySlug(slug)
	interventionService = InterventionService(request.user)
	interventions = interventionService.getInterventions(project, 'twitter', '-created_date')
	mostRecentIntervention = None

	if (len(interventions) > 0):
		mostRecentIntervention = interventions[0]

	if request.method == 'GET':
		return render(request, 'project/conversations_manager_facebook.html', { 'project': projectService.getProjectBySlug(slug), 'interventions': interventions, 'mostRecentIntervention': mostRecentIntervention, 'active_tab': 'conversations_twitter' })


#likely not called
@login_required
def createTwitterAudience(request, slug):
	#need form data, including csv or direct audience input
	if request.method == 'POST':
		name = request.POST.get('audience_name');
		description = request.POST.get('audience_description')
		audience_type_id = request.POST.get('audience_type')

		projectService = ProjectService(request.user)
		project = projectService.getProjectBySlug(slug)
		audienceService = AudienceService(request.user)

		audienceType = audienceService.getAudienceType(audience_type_id)
		audience = None

		if audienceType.name == 'seed' :
			csv_file = request.FILES['csv']
			path = handle_uploaded_audience_file(csv_file)
			try:
				audience = audienceService.createAudienceFromCsv(file, project, audienceName=name, audienceDescription=description)
				#kick off background jobs
				logger.info('kicking off createSeedHashtags background process')
				proc.createSeedHashtags(audience.id, request.user.id)
				logger.info('createSeedHashtags background process should have been called')
				data = serializers.serialize('json', [audience])
				return JsonResponse(data, safe=False)
			except ValueError as e:
				print(e)
				data = { 'error': e }
				return JsonResponse(data, safe=False)


@login_required
def createTwitterIntervention(request, slug):
	#need form data, including csv or direct audience input
	if request.method == 'POST':
		name = request.POST.get('audience_name');
		description = request.POST.get('audience_description')
		audience_type_id = request.POST.get('audience_type')

		projectService = ProjectService(request.user)
		project = projectService.getProjectBySlug(slug)
		audienceService = AudienceService(request.user)

		audienceType = audienceService.getAudienceType(audience_type_id)
		audience = None

		if audienceType.name == 'seed' :
			csv_file = request.FILES['csv']
			path = handle_uploaded_audience_file(csv_file)
			try:
				audience = audienceService.createAudienceFromCsv(file, project, audienceName=name, audienceDescription=description)
				#kick off background jobs
				data = serializers.serialize('json', [audience])
				return JsonResponse(data, safe=False)
			except ValueError as e:
				print(e)
				data = { 'error': e }
				return JsonResponse(data, safe=False)


@login_required
def createFacebookIntervention(request, slug):
	#need form data, including csv or direct audience input
	if request.method == 'POST':
		name = request.POST.get('audience_name');
		description = request.POST.get('audience_description')
		audience_type_id = request.POST.get('audience_type')

		projectService = ProjectService(request.user)
		project = projectService.getProjectBySlug(slug)
		audienceService = AudienceService(request.user)

		audienceType = audienceService.getAudienceType(audience_type_id)
		audience = None

		if audienceType.name == 'seed' :
			csv_file = request.FILES['csv']
			path = handle_uploaded_audience_file(csv_file)
			try:
				audience = audienceService.createAudienceFromCsv(file, project, audienceName=name, audienceDescription=description)
				#kick off background jobs
				data = serializers.serialize('json', [audience])
				return JsonResponse(data, safe=False)
			except ValueError as e:
				print(e)
				data = { 'error': e }
				return JsonResponse(data, safe=False)


def testBackgroundTask(request):
	tasks.testGetFollowers(request.user.id)
	return JsonResponse({ 'is_task_running': True }, safe=False)



def handle_uploaded_audience_file(f):
	path = 'commons/audience_files/' + f.name
	with open(path, 'wb+') as destination:
		for chunk in f.chunks():
			destination.write(chunk)
	return path

def handle_uploaded_intervention_file(f):
	path = 'commons/intervention_files/' + f.name
	with open(path, 'wb+') as destination:
		for chunk in f.chunks():
			destination.write(chunk)
	return path




	#check the audience type
	#if it's a seed audience, then do a try/except on 
	#audienceService.createAudienceFromCsv




	#create audience in db, then kick off twitter background task

#def twitterWebhook(request):
	#do webhook stuff
	#call twitter social module
	#call relevant services