from background_task import background
from multiprocessing import Process, Queue
import queue
import time
from django.conf import settings
from django.contrib.auth.models import User
from commons.services.user import UserService
from commons.social.twitter import TweeAccount
from commons.services.project import ProjectService
from commons.services.audience import AudienceService
from commons.services.hashtag import HashtagService
from commons.services.apiAccount import ApiAccountService
from commons.models import Audience, AudienceMember
from commons.services.apiAccount import ApiAccountService
from commons.services.intervention import InterventionService
from commons.services.conversation import ConversationService
from datetime import datetime
from pytz import timezone
import logging

logger = logging.getLogger(__name__)


def getAudienceMemberQueue(audience):

	q = Queue()

	for member in audience.members.all():
		q.put(member)

	return q


def fetchAndStoreFollowers(audienceService, basePopulation, membersQueue, tweeAccount):

		while not membersQueue.empty():

			try:
				member = membersQueue.get(False)

				followsId = member.id

				logger.info('fetching ' + member.username + '\'s twitter followers')

				followerIds = tweeAccount.getFollowerIds(member.username)

				logger.info('fetched ' + str(len(followerIds)) + ' follower ids from ' + member.username + '\'s twitter account')

				for followerId in followerIds:
					#check if follower already exists
					audienceMember = audienceService.getAudienceMemberBySocialId(followerId, basePopulation)

					if audienceMember is not None:

						#check if follower meets lead criteria
						user = tweeAccount.getUser(followerId)

						if user.followers_count >= settings.MIN_TWITTER_FOLLOWERS and user.followers_count <= settings.MAX_TWITTER_FOLLOWERS and user.statuses_count >= settings.MIN_NUM_TWEETS:
							#get the user's timeline
							#check if they have retweeted at least one of the seed handles
							#if so, get the user's ideology score from the seed handle retweets
							#get the retweets count and ideology score at the same time
							seedAudience = audienceService.getAudience(basePopulation.parent_id)

							seedAudienceMembers = seedAudience.members.all()

							seedHandles = []

							for member in seedAudienceMembers:
								seedHandles.extend(member.username)

							numSeedRetweets = 0
							ideologies = []
							statusCount = 0

							for status in tweeAccount.getUserTimeline(user.screen_name):

								if statusCount > settings.MAX_NUM_TWEETS:
									break

							#check if tweet is retweet
							#if so, check who it's a retweet of 
							#if the retweet handle is in seedHandles,
							#then increment numSeedRetweets and add
							#the seedhandle's ideology score to the list
								if hasattr(status, 'retweeted_status'):
									originalAuthor = status.retweeted_status.user.screen_name
									if originalAuthor in seedHandles:
										numSeedRetweets = numSeedRetweets + 1
										seedAudienceMember = seedAudienceMembers.filter(audience=seedAudience, username=originalAuthor).first()
										ideologyScore = audienceService.getIdeologyScore(seedAudienceMember)
										ideologies.extend(ideologyScore.score)


								statusCount = statusCount + 1

							if numSeedRetweets > 0 and len(ideologies) > 0:
								ideologyScore = sum(ideologies) / len(ideologies)
								audienceService.createAudienceMember(basePopulation, None, socialId=followerId, followsId=followsId, ideologyScore=ideologyScore)

				logger.info('follower ids added to db')

			except queue.Empty as e:
				logger.error('empty queue error')
				break



def fetchAndStoreSeedHashtags(hashtagService, audienceMember, tweeAccount):


	#allow for *overwriting hashtags
	#hashtags shouldn't be erased for reporting
	#reasons. Instead, consider providing 
	#a "copy audience from base population" function
	try:
		logger.info('fetching ' + audienceMember.username + '\'s tweeted hashtags')

		hashtags = tweeAccount.getTweetedHashtags(audienceMember.username, pastNumDays=7)

		logger.info('fetched ' + str(len(hashtags)) + ' hashtags from ' + audienceMember.username + '\'s twitter account')

		for hashtag in hashtags:
			hashtagService.createHashtag(audienceMember, 'regular', hashtag)

		logger.info('hashtags added to db')

	except Exception as e:
		logger.error('error: ' + str(e))

	return None


#limit to top 10 hashtags
def fetchAndStoreCoOccurringHashtags(hashtagService, audienceMember, tweeAccount):

	#get the regular hashtags of the audience member
	#take the first 20 hashtags for the co-occuring query
	hashtags = hashtagService.getHashtags(audienceMember, 'regular')



	hashtagsSet = set()

	for tag in hashtags:
		if (len(hashtagsSet) >= 20):
			break
		hashtagsSet.add(tag.hashtag)

	print('set length: ' + str(len(hashtagsSet)))


	logger.info('fetching ' + audienceMember.username + '\'s tweeted co-occurring hashtags')

	coOccuringHashtags = tweeAccount.getCoOccuringHashtags(hashtagsSet)

	logger.info('fetched ' + str(len(hashtags)) + ' co-occuring hashtags from ' + audienceMember.username + '\'s tweeted hashtags')

	for hashtag in coOccuringHashtags:
		hashtagService.createHashtag(audienceMember, 'co-occurring', hashtag)

	logger.info('hashtags added to db')


	#need to check limit of hashtags that can be searched for
	#UPDATE - it's a 500 char limit, including operators and spaces

	#this process needs more thought to limit 
	#the number of hashtags returned
	return None



def createBasePopulationProcess(seedAudienceId, userId):
	#get the user
	userService = UserService()
	user = userService.getUser(userId)
	projectService = ProjectService(user)
	audienceService = AudienceService(user)

	seedAudience = audienceService.getAudience(seedAudienceId)
	
	#check to see if base population already exists
	basePopulation = audienceService.getAudienceByParentId(seedAudience.id, audienceTypeName='base population') #add function args

	if basePopulation is not None:
		error = 'Base population already exists'
		logger.error(error)
		return None

	basePopulation = audienceService.createAudience('base population – ' + seedAudience.name , 'the largest group of people considered for an intervention', seedAudience.project, audienceTypeName='base population', parentId=seedAudience.id, statusName='processing')

	seedAudienceMembersQueue = getAudienceMemberQueue(seedAudience)

	#start 4 twitter processes with different api accounts and pass
	#the queue to these processes so each can call queue.get()
	#to get the next queue member

	#start the 4 processes here
	apiAccountService = ApiAccountService()

	twitterApiAccounts = apiAccountService.getApiAccounts('twitter')


	for account in twitterApiAccounts:
		if not account.is_reserved:
			tweeAccount = TweeAccount(account.access_token, account.access_token_secret, account.consumer_key, account.consumer_secret)
			p = Process(target=fetchAndStoreFollowers, args=(audienceService, basePopulation, seedAudienceMembersQueue, tweeAccount))
			p.start()

	while not seedAudienceMembersQueue.empty():
		pass


	logger.info('Base Population successfully created')
	audienceService.setAudienceStatus(basePopulation, 'ready')
	#notify user
	message = 'Hi ' + user.first_name + ', The base population is ready for the new seed audience you created named ' + seedAudience.name + '. This base population has ' + str(len(basePopulation.members.all())) + ' members'
	userService.notifyUser(user, 'Base Population Ready For Audience – ' + seedAudience.name, message)






def createSeedHashtagsProcess(seedAudienceId, userId):
	logger.info('createSeedHashtagsProcess called')
	#get the user
	userService = UserService()
	user = userService.getUser(userId)
	audienceService = AudienceService(user)
	hashtagService = HashtagService(user)
	tweeAccount = TweeAccount()

	seedAudience = audienceService.getAudience(seedAudienceId)

	for member in seedAudience.members.all():
		fetchAndStoreSeedHashtags(hashtagService, member, tweeAccount)

	#update seed audience hashtags status
	audienceService.setAudienceJobFinished(seedAudience, 'seed_hashtags', True)

	#notify user
	message = 'Hi ' + user.first_name + ', The seed handle hashtags are ready for the new seed audience you created named ' + seedAudience.name 
	userService.notifyUser(user, 'Seed Hashtags Ready For Audience – ' + seedAudience.name, message)

	createCoOccurringHashtagsProcess(seedAudienceId, userId)





def createCoOccurringHashtagsProcess(seedAudienceId, userId):
	#get the user
	userService = UserService()
	user = userService.getUser(userId)
	audienceService = AudienceService(user)
	hashtagService = HashtagService(user)
	tweeAccount = TweeAccount()

	seedAudience = audienceService.getAudience(seedAudienceId)

	for member in seedAudience.members.all():
		fetchAndStoreCoOccurringHashtags(hashtagService, member, tweeAccount)

		#update seed audience hashtags status
	audienceService.setAudienceJobFinished(seedAudience, 'co_hashtags', True)

	#notify user
	message = 'Hi ' + user.first_name + ', The co-occurring hashtags are ready for the new seed audience you created named ' + seedAudience.name 
	userService.notifyUser(user, 'Co-occurring Hashtags Ready For Audience – ' + seedAudience.name, message)

	createBasePopulationProcess(seedAudienceId, userId)



def createSeedHashtags(seedAudienceId, userId):
	logger.info('createSeedHashtags called')
	p = Process(target=createSeedHashtagsProcess, args=(seedAudienceId, userId))
	p.start()

def createBasePopulation(seedAudienceId, userId):
	logger.info('createBasePopulation called')
	p = Process(target=createBasePopulationProcess, args=(seedAudienceId, userId))
	p.start()




#add the intervention processes



def runInterventionProcess(interventionId, userId):
	print('runInterventionProcess called')
	userService = UserService()
	user = userService.getUser(userId)
	interventionService = InterventionService(user)
	intervention = interventionService.getIntervention(interventionId)
	apiAccountService = ApiAccountService()
	account = apiAccountService.getApiAccount('the_commons')
	#dedicate the commons twitter account to streaming
	tweeAccount = TweeAccount(account.access_token, account.access_token_secret, account.consumer_key, account.consumer_secret)

	hashtags = interventionService.getInterventionHashtags(intervention)

	statusQueue = Queue()

	stream = tweeAccount.streamTweets(hashtags, statusQueue) #not running async - needs to be async

	print('stream running')

	eastern = timezone('US/Eastern')

	while datetime.now(eastern) < intervention.end_time and interventionService.getStatus(intervention).name == 'in progress':

		while not statusQueue.empty():
			status = statusQueue.get(False)
			print('status id: ' + status.id_str)
			print('status hashtags: ' + str(tweeAccount.getHashTagsFromStatus(status)))
			print('intervention hashtags: ' + str(hashtags))
			print('status contains hashtags: ' + str(tweeAccount.containsHashtags(status, hashtags)))
			if tweeAccount.containsHashtags(status, hashtags):
				#do processing based on intervention rules #update
				handleInterventionStreamStatusObj(intervention, status, user.id)

		print('sleeping for 30s')
		time.sleep(30)

	stream.stop()


def handleInterventionStreamStatusObj(intervention, statusObj, userId):
	#apply the rules of the intervention to the statusObj
	#if match, create conversation and *assign facilitator

	#check if tweet author is in the base population 
	print('handleInterventionStreamStatusObj called')

	userService = UserService()
	user = userService.getUser(userId)
	audienceService = AudienceService(user)
	interventionService = InterventionService(user)
	conversationService = ConversationService(user)

	if audienceService.isInBasePopulation(intervention.audience, statusObj.user.screen_name):
		#make sure the tweet author is not a Twitter authenticated
		#member of the platform
		if not userService.isSocialAuthUser(statusObj.user.screen_name, 'twitter'):
			#apply the intervention rules, if there are any
			#skip this for now
			audienceMember = audienceService.getAudienceMemberBySocialId(statusObj.user.id_str, intervention.audience)

			#inject the hashtag(s) they used into the content

			#get the next available facilitator
			facilitator = userService.getNextAvailableFacilitator(intervention)

			if facilitator is not None:
				accessToken = userService.getTwitterAccessToken(facilitator)
				tweeAccount = TweeAccount(accessToken['oauth_token'], accessToken['oauth_token_secret']) 

				groupMember = None
				#create a conversation
				conversation = conversationService.createConversation(facilitator, 'twitter', intervention, tweeAccount.getTweetUrl(statusObj))

				conversationMessage = conversationService.createConversationMessage(statusObj.user.screen_name, conversation, statusObj.id_str, tweeAccount.getFullText(statusObj), tweeAccount.getTweetUrl(statusObj))
				#the first message of the conversation is the content of the statusObj
				#(need to handle emoticons)
				#the second message of the conversation is the reply tweet to the statusObj
				#(this requires getting the hashtags intervention content (randomly))
				#(use the first hashtag in the tweet as the injection hashtag)
				#add the tweet author to the treatment group
				#(create the treatment and control groups when
				# the intervention is created)
				# notify the facilitator via email (only when they need to respond) when
				# the tweet author responds
				hashtags = interventionService.getInterventionHashtags(intervention)
				hashtag = tweeAccount.getFirstMatchingHashtag(statusObj, hashtags)

				messageGroup = interventionService.getRandomTwitterMessage(intervention, hashtag) 

				message = interventionService.prepareMessage(messageGroup, hashtag, statusObj.user.screen_name) 

				#sleep for 60s so it looks like a person is replying instead of a program
				time.sleep(60)

				newStatusObj = tweeAccount.tweet(message, inReplyToTweetId=statusObj.id_str, username=statusObj.user.screen_name)
				conversationService.createConversationMessage(newStatusObj.user.screen_name, conversation, newStatusObj.id_str, tweeAccount.getFullText(newStatusObj), tweeAccount.getTweetUrl(newStatusObj), repliedToUsername=statusObj.user.screen_name, repliedToSocialConversationId=statusObj.id_str)
				conversationService.createConversationReply(conversationMessage, newStatusObj.id_str) 

				groupMember = interventionService.createExperimentGroupMember(intervention, 'treatment', audienceMember)

			else:

				#else add the tweet author to the control group
				groupMember = interventionService.createExperimentGroupMember(intervention, 'control', audienceMember)


			#get who the group member follows and put in db
			followsIds = tweeAccount.getFriendsIds(statusObj.user.screen_name)

			logger.info('fetched ' + str(len(followsIds)) + ' follows(friends) ids from ' + statusObj.user.screen_name + '\'s twitter account')

			for followsId in followsIds:
				interventionService.createExperimentGroupMemberFollows(groupMember, followsId) 

			logger.info('follows ids added to db')

			print('handleInterventionStreamStatusObj ended')

			#should add ExperimentGroupMemberIdeologyScore too (this concept is broken because of the shortness of the intervention period and 
			# because retweet behavior is subjective within the scope of the seed handles
			# e.g. (what if a candidate stops retweeting certain seed handles who are democratic
			# and starts retweeting handles that are more conservative that are not seed handles?))





def runIntervention(interventionId, userId) :
	p = Process(target=runInterventionProcess, args=(interventionId, userId))
	p.start()


def updateActiveConversationsProccess():
	userService = UserService()
	sysUser = userService.getSystemUser()
	interventionService = InterventionService(sysUser)
	conversationService = ConversationService(sysUser)
	tweeAccount = TweeAccount()

	interventions = interventionService.getInterventionsByStatus('in progress') #test

	activeConversations = []

	for intervention in interventions:
		activeConversations.extend(intervention.conversations.all().filter(is_closed=False))

	for conversation in activeConversations:

		facilitator = conversation.user
		#get any conversation messages
		#for this conversation that
		#have no conversationreplies
		#and have a non-none 
		#replied_to_username and non-none
		#replied_to_social_conversation_id
		responseNeededMessages = conversationService.getResponseNeededConversationMessages(conversation) #test

		#for each message, go to twitter and get the timeline of
		#the replied to username since the replied_to_social_conversation_id
		#return any statusObjs that have a in_reply_to_status_id that matches
		#social_conversation_id
		for message in responseNeededMessages:
			replies = tweeAccount.getTweetReplies(message.social_conversation_id, message.replied_to_username, message.replied_to_social_conversation_id) #test
			isNotifyFacilitator = False

			for reply in replies:
				#create conversation reply recs for this message
				#create new conversation message for this conversation
				#ideally send one email that has conversation info 
				print('reply: ' + tweeAccount.getFullText(reply))
				conversationService.createConversationReply(message, reply.id_str)
				conversationService.createConversationMessage(reply.user.screen_name, conversation, reply.id_str, tweeAccount.getFullText(reply), tweeAccount.getTweetUrl(reply), repliedToUsername=message.author, repliedToSocialConversationId=message.social_conversation_id)

				if reply.user.screen_name != facilitator.social_auth.all().first().extra_data['access_token']['screen_name']:
					isNotifyFacilitator = True

			if len(replies) > 0 and isNotifyFacilitator:
				#send email to facilitator
				message =  'Hi ' + facilitator.first_name + ', ' + reply.user.screen_name + ' has replied to your intervention tweet. Please respond to the conversation on twitter. Here is the link '
				htmlMessage = message

				message += conversation.link
				htmlMessage += '<a href="' + conversation.link + '">' + conversation.link + '</a>'
				userService.notifyUser(facilitator, '[commons project] You have a new conversation message', message, htmlMessage) #include link in email
			else:
				print('No replies from twitter candidate')




		#if so, update the db records
		#notify the conversation user via email
		#(if the author is not them)
		#that someone has responded to their conversation






	

def updateActiveConversations():
	p = Process(target=updateActiveConversationsProccess)
	p.start()








