from commons.models import Intervention, InterventionHashtag, SocialPlatform, InterventionStatus, ExperimentGroup, ExperimentGroupType, ExperimentGroupMember, InterventionContentGroupType, InterventionContentGroup, InterventionMessageGroup, InterventionMessage, InterventionMessageGroupHashtag, ExperimentGroupMemberFollows
import random
import csv


class InterventionService():

	def __init__(self, user = None):

		self.user = user

	def createIntervention(self, project, socialPlatformName, audience, description, startTime, endTime, **options):
		#the default status will always be "scheduled",
		#even if the user selects "now" as the start_time
		#the cron job that handles running interventions
		#will look for interventions that have been
		#scheduled and see if now is >= the scheduled
		#date time. if so, it will start the intervention
		#the job can run once an hour

		#the hypothesis will be optional

		socialPlatform = SocialPlatform.objects.filter(name=socialPlatformName).first()
		status = InterventionStatus.objects.filter(name='scheduled').first()

		intervention = Intervention(project=project, social_platform=socialPlatform, audience=audience, description=description, 
			start_time=startTime, end_time=endTime, status=status,
			created_by=self.user.id, updated_by=self.user.id)


		if options is not None:

			if 'hypothesis' in options:
				intervention.hypothesis = options.get('hypothesis')

		intervention.save()

		self.createExperimentGroup(intervention, 'treatment')
		self.createExperimentGroup(intervention, 'control')


		return intervention


	def createInterventionHashtagsFromCsv(self, csvFile, intervention):

		contentGroupType = InterventionContentGroupType.objects.filter(name="hashtags").first()

		contentGroup = InterventionContentGroup.objects.create(intervention=intervention,
																group_type=contentGroupType,
																created_by=self.user.id,
																updated_by=self.user.id
																)

		dict = csv.DictReader(csvFile)

		for row in dict:

			for key in row:
				print('key : ' + str(key))
				print ('val : ' + str(row[key]))

			messageGroup = InterventionMessageGroup.objects.create(content_group=contentGroup, created_by=self.user.id, updated_by=self.user.id)

			hashtagsStr = row['#']

			hashtags = hashtagsStr.split(';')

			for hashtag in hashtags:
				InterventionMessageGroupHashtag.objects.create(message_group=messageGroup, hashtag=hashtag, created_by=self.user.id, updated_by=self.user.id)

			if row['A']:
				InterventionMessage.objects.create(message_group=messageGroup, text=row['A'], created_by=self.user.id, updated_by=self.user.id)

			if row['B']:
				InterventionMessage.objects.create(message_group=messageGroup, text=row['B'], created_by=self.user.id, updated_by=self.user.id)

			if row['C']:
				InterventionMessage.objects.create(message_group=messageGroup, text=row['C'], created_by=self.user.id, updated_by=self.user.id)


		return intervention


	def createExperimentGroup(self, intervention, groupTypeName):
		experimentGroupType = ExperimentGroupType.objects.filter(name=groupTypeName).first()
		print('exp group type name ' + experimentGroupType.name)
		return ExperimentGroup.objects.create(intervention=intervention, group_type=experimentGroupType, created_by=self.user.id, updated_by=self.user.id)


	def createExperimentGroupMember(self, intervention, groupTypeName, audienceMember):
		experimentGroupType = ExperimentGroupType.objects.filter(name=groupTypeName).first()
		group = ExperimentGroup.objects.filter(intervention=intervention, group_type=experimentGroupType).first()
		return ExperimentGroupMember.objects.create(experiment_group=group, audience_member=audienceMember, created_by=self.user.id, updated_by=self.user.id)

	def createExperimentGroupMemberFollows(self, groupMember, followsId):
		return ExperimentGroupMemberFollows.objects.create(member=groupMember, follows_id=followsId, created_by=self.user.id, updated_by=self.user.id)



	def addRule(intervention, ruleId):

		return None

	def getInterventionsByStatus(self, statusName):
		status = InterventionStatus.objects.filter(name=statusName).first()
		return Intervention.objects.filter(status=status)

	def updateStatus(self, intervention, statusName):
		status = InterventionStatus.objects.filter(name=statusName).first()
		if status is not None:
			intervention.status = status
			intervention.save()


	def getInterventionHashtags(self, intervention):
		contentGroupType = InterventionContentGroupType.objects.filter(name='hashtags').first()
		contentGroup = InterventionContentGroup.objects.filter(intervention=intervention, group_type=contentGroupType).first()
		messageGroups = InterventionMessageGroup.objects.filter(content_group=contentGroup)

		hashtags = []

		for group in messageGroups:
			for groupHashtag in group.hashtags.all():
				hashtags.append(groupHashtag.hashtag.strip())

		return hashtags


	def getIntervention(self, id):
		return Intervention.objects.filter(id=id).first()

	def getStatus(self, intervention):
		return Intervention.objects.filter(id=intervention.id).first().status

	def getInterventions(self, project, socialPlatformName, orderBy=None):
		socialPlatform = SocialPlatform.objects.filter(name=socialPlatformName).first()

		if orderBy is not None:
			return Intervention.objects.filter(project=project, social_platform=socialPlatform).order_by(orderBy)
		else:
			return Intervention.objects.filter(project=project, social_platform=socialPlatform)

	def getRandomTwitterMessage(self, intervention, hashtag, groupTypeName='hashtags'):
		contentGroupType = InterventionContentGroupType.objects.filter(name=groupTypeName).first()
		contentGroup = InterventionContentGroup.objects.filter(intervention=intervention, group_type=contentGroupType).first()
		messageGroups = InterventionMessageGroup.objects.filter(content_group=contentGroup)

		if len(messageGroups) > 0:
			matchingMessageGroups = [] 
			for messageGroup in messageGroups:
				for messageGroupHashtag in messageGroup.hashtags.all():
					if hashtag in messageGroupHashtag.hashtag:
						matchingMessageGroups.append(messageGroup)
						break

			upperbound = len(matchingMessageGroups) - 1
			if upperbound < 0:
				upperbound = 0

			randomMessageGroupSelection = random.randint(0, upperbound)
			return matchingMessageGroups[randomMessageGroupSelection]
		else:
			return None

	def prepareMessage(self, messageGroup, hashtag, username):
		message = ''

		for template in messageGroup.messages.all():
			message += template.text.replace('#', '#' + hashtag).replace('@', '@' + username).replace('_', '')
			message += ' '

		return message.strip()

		








	