#import models
from commons.models import User, Project, ProjectMember, ProjectAccessLevel, ProjectRequestStatus, ProjectAccessRequestLog, ProjectAccessRequest



class ProjectService():

	def __init__(self, user):
		self.user = user

	def hasAccess(self, project, user = None):
		print('# of project members: ', len(project.members.all()))
		if user is None:
			user = self.user
		return len(project.members.filter(user=user)) == 1

	def createProject(self, name, shortDescription, startDate, endDate, **options):
		project = Project(name=name, short_description=shortDescription, 
			start_date=startDate, end_date=endDate, is_admin_only = False,
			project_owner=self.user, created_by=self.user.id, updated_by=self.user.id)

		url_slug = name.replace(' ', '-')
		url_slug = url_slug.replace('.', '-')
		url_slug = url_slug.lower()
		project.url_slug = url_slug

		if options is not None:
			if 'longDescription' in options:
				project.long_description = options.get('longDescription')

			if 'isAdminOnly' in options:
				project.is_admin_only = options.get('isAdminOnly')

		project.save()

		self.addMember(self.user, project, "edit")

		return project

	def createProjectAccessRequest(self, project, requestStatusName, **options):
		requestStatus = ProjectRequestStatus.objects.filter(name=requestStatusName).first()
		accessLevel = None
		user = self.user

		if options is not None:
			if 'accessLevelName' in options:
				accessLevel = ProjectAccessLevel.objects.filter(name=options.get('accessLevelName'))

			if 'user' in options:
				user = options.get('user')

		if accessLevel is None:
			accessLevel = ProjectAccessLevel.objects.filter(name='read only').first()

		ProjectAccessRequestLog.objects.create(user=user, project=project, access_level=accessLevel,
			request_status=requestStatus, created_by=user.id, updated_by=user.id)

		return ProjectAccessRequest.objects.create(user=user, project=project, access_level=accessLevel,
			request_status=requestStatus, created_by=user.id, updated_by=user.id)


	def addMember(self, user, project, accessLevelName = "read only"):
		#should check if member already exists

		if not self.hasAccess(project, user):
			#use default access level
			accessLevel = ProjectAccessLevel.objects.get(name=accessLevelName);
			projectMember = ProjectMember(project=project, user=user, access_level=accessLevel, created_by=self.user.id, updated_by=self.user.id)
			projectMember.save()
			project.members.add(projectMember)

	def getAllProjects(self):
		return Project.objects.all()

	def getProjectMemberships(self, user = None):
		if user is not None:
			return ProjectMember.objects.filter(user=user)

		return ProjectMember.objects.filter(user=self.user)

	def getProjectBySlug(self, slug):
		return Project.objects.filter(url_slug=slug).first()

	def getProject(self, id):
		return Project.objects.filter(id=id).first()

