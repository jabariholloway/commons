from commons.models import ApiAccount, SocialPlatform

class ApiAccountService():

	def getApiAccount(self, accountName):
		return ApiAccount.objects.filter(name=accountName).first()

	def getApiAccounts(self, socialPlatformName):
		socialPlatform = SocialPlatform.objects.filter(name=socialPlatformName).first()
		return ApiAccount.objects.filter(social_platform=socialPlatform)


