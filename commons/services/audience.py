from commons.models import SocialPlatform, AudienceType, Audience, AudienceMember, ProcessStatus, AudienceMemberIdeologyScore, HashtagType
import csv
import os
import itertools
from datetime import datetime

class AudienceService():

	def __init__(self, user):
		self.user = user


	def createAudience(self, name, description, project, **options):

		socialPlatform = None
		audienceType = None
		parentId = None
		status = None

		if options is not None:

			if 'socialPlatformName' in options:
				socialPlatform = SocialPlatform.objects.get(name=options.get('socialPlatformName'))

			if 'audienceTypeName' in options:
				audienceType = AudienceType.objects.get(name=options.get('audienceTypeName'))

			if 'parentId' in options:
				parentId = options.get('parentId')

			if 'statusName' in options:
				status = ProcessStatus.objects.get(name=options.get('statusName'))


		if socialPlatform is None:
			socialPlatform = SocialPlatform.objects.get(name='twitter')

		if audienceType is None:
			audienceType = AudienceType.objects.get(name='seed')

		audience = Audience(social_platform=socialPlatform, audience_type=audienceType, 
			name=name, description=description, project=project, status=status, created_by=self.user.id, updated_by=self.user.id)

		if parentId is not None:
			audience.parent_id = parentId

		audience.save()

		return audience


	def createAudienceMember(self, audience, username, **options):

		audienceMember = AudienceMember(audience=audience, username=username, 
			created_by=self.user.id, updated_by=self.user.id)

		ideologyScore = None

		if options is not None:
			if 'followsId' in options:
				audienceMember.follows_id = options.get('followsId')

			if 'socialId' in options:
				audienceMember.social_id = options.get('socialId')

			if 'ideologyScore' in options:
				ideologyScore = options.get(ideologyScore)

		audienceMember.save()

		if ideologyScore is not None:
			self.createIdeologyScore(audienceMember, ideologyScore)

		return audienceMember


	def createAudienceFromString(self, audienceString, project, audienceName, audienceDescription, **options):

		audience = self.createAudience(audienceName, audienceDescription, project, **options)

		for i, line in enumerate(audienceString.splitlines()):
			if i > 0:
				vals = line.split(',')
				member = audience.members.create(
							username=vals[0].replace("@",""),
							created_by=self.user.id,
							updated_by=self.user.id
						)

				member.ideologyScores.create(
							score=vals[1].strip(),
							created_by=self.user.id,
							updated_by=self.user.id
						)

		return audience





	def createAudienceFromCsv(self, csvFile, project, **options):
		#createAudience - if no name specified, default to file name'
		#where n is the next db id
		#make sure to close file 
		#default to twitter seed audience

		#check if file is formatted properly
		if self.csvHasSeedAudienceHeader(csvFile):
			audienceName = self.getDefaultAudienceName(csvFile)
			audienceDescription = ''

			#create audience
			socialPlatform = None
			audienceType = None

			numLines = sum(1 for line in csvFile if line.rstrip())

			if numLines >= 2:
				csvFile.seek(0)
				csvFile = self.formatCsvSeedAudienceHeader(csvFile)
				

				if options is not None:
					if 'audienceName' in options:
						audienceName = options.get('audienceName')

					if 'audienceDescription' in options:
						audienceDescription = options.get('audienceDescription')
					

				audience = self.createAudience(audienceName, audienceDescription, project, **options)

				#add members to audience and return audience

				dict = csv.DictReader(csvFile)

				for row in dict:
					member = audience.members.create(
							username=row['username'].replace("@",""),
							created_by=self.user.id,
							updated_by=self.user.id
						)

					member.ideologyScores.create(
							score=float(row['score']),
							created_by=self.user.id,
							updated_by=self.user.id
						)

				return audience

			else:
				raise ValueError('The csv file must have at least one row of data')

			audience 

		else:
			raise ValueError('The csv file must contain "Username,Score" as its header')


	def createIdeologyScore(audienceMember, ideologyScore):
		return AudienceMemberIdeologyScore.create(audience_member=audienceMember, 
			score=ideologyScore, created_by=self.user.id, updated_by=self.user.id)


	def csvHasSeedAudienceHeader(self, csvFile):
		firstLine = next(csvFile).lower()
		csvFile.seek(0)
		return 'username' in firstLine and 'score' in firstLine

	def formatCsvSeedAudienceHeader(self, csvFile):
		return itertools.chain([next(csvFile).lower()], csvFile)

	def getDefaultAudienceName(self, csvFile):
		base = os.path.basename(csvFile.name)
		return os.path.splitext(base)[0]

	def getAudiences(self, project, socialPlatformName, **options):
		socialPlatform = SocialPlatform.objects.filter(name=socialPlatformName).first()
		isDeleted = False
		audiences = Audience.objects.filter(project=project, social_platform=socialPlatform, is_deleted=isDeleted)
		

		if options is not None:
			if 'audienceTypeName' in options:
				audienceType = AudienceType.objects.filter(name=options.get('audienceTypeName')).first()
				audiences = Audience.objects.filter(project=project, social_platform=socialPlatform, audience_type=audienceType, is_deleted=isDeleted)

		if audiences is not None:
			for audience in audiences:
				audience.parent = None
				if audience.parent_id is not None:
					audience.parent = self.getAudience(audience.parent_id)

		return audiences

	def getAudience(self, id):
		return Audience.objects.filter(id=id).first()

	def getAudienceByParentId(self, parentId, **options):

		if options is not None:
			if 'audienceTypeName' in options:
				audienceType = AudienceType.objects.filter(name=options.get('audienceTypeName')).first() 
				return Audience.objects.filter(parent_id=parentId, audience_type=audienceType).first()

		return Audience.objects.filter(parent_id=parentId).first()


	def getAudienceTypes(self):
		return AudienceType.objects.all()

	def getAudienceTypeByName(self, name):
		return AudienceType.objects.filter(name=name).first()

	def getAudienceType(self, id):
		return AudienceType.objects.filter(id=id).first()

	def getAudienceMember(self, id):
		return AudienceMember.objects.filter(id=id).first()


	def getAudienceMemberBySocialId(self, socialId, audience):
		return AudienceMember.objects.filter(social_id=socialId, audience=audience).first()

	def getIdeologyScore(self, audienceMember):
		return AudienceMemberIdeologyScore.objects.filter(audience_member=audienceMember).first()

	def setAudienceStatus(self, audience, statusName):
		status = ProcessStatus.objects.filter(name=statusName).first()
		audience.status = status
		audience.updated_by = self.user.id
		audience.updated_at = datetime.now
		audience.save()

	def setAudienceJobFinished(self, audience, jobItemName, isFinished):

		if (jobItemName == 'seed_hashtags'):
			
			audience.is_seed_hashtags_ready = isFinished

		elif(jobItemName == 'co_hashtags'):

			audience.is_co_hashtags_ready = isFinished

		elif(jobItemName == 'based_population'):

			audience.is_base_population_ready = isFinished

		audience.save()

		return audience




	def isInBasePopulation(self, audience, username):
		basePopulation = None
		audienceTypeSeed = self.getAudienceTypeByName('seed')
		audienceTypeBasePopulation = self.getAudienceTypeByName('base population')
		if audience.audience_type == audienceTypeBasePopulation :
			basePopulation = audience
		elif audience.audience_type == audienceTypeSeed :
			basePopulation = Audience.objects.filter(parent_id=audience.id, audience_type=audienceTypeBasePopulation).first()

		if basePopulation is None:
			return False

		return len(basePopulation.members.filter(username=username)) > 0


	def getSeedHashtagsCsvData(self, seedAudience):

		csvData = []

		header = ['Hashtag', 'Type', 'Avg_Ideology_Score', 'Num_Times_Tweeted']

		csvData.append(header)

		hashtagTypeRegular = HashtagType.objects.filter(name='regular').first()
		hashtagTypeCoOccurring = HashtagType.objects.filter(name='co-occurring').first()

		seedHashtagsMap = {}

		for member in seedAudience.members.all():

			for hashtagObj in member.hashtags.filter(hashtag_type=hashtagTypeRegular):

				scoreObj = self.getIdeologyScore(member)

				if hashtagObj.hashtag in seedHashtagsMap:

					seedHashtagsMap[hashtagObj.hashtag]['count'] = seedHashtagsMap[hashtagObj.hashtag]['count'] + 1
					seedHashtagsMap[hashtagObj.hashtag]['scores'].append(scoreObj.score)


				else:
					seedHashtagsMap[hashtagObj.hashtag] = { 'count': 1, 'scores': [scoreObj.score] }


		#sort the map by count desc
		sortedSeedHashtags = sorted(seedHashtagsMap.items(), key=lambda kv: kv[1]['count'], reverse=True)

		for i, hashtagRow in enumerate(sortedSeedHashtags):
			if i >= 100:
				break
			scores = hashtagRow[1]['scores']
			csvData.append([ hashtagRow[0], hashtagTypeRegular.name, sum(scores)/len(scores), hashtagRow[1]['count']])


		coHashtagsMap = {}

		for member in seedAudience.members.all():

			for hashtagObj in member.hashtags.filter(hashtag_type=hashtagTypeCoOccurring):

				scoreObj = self.getIdeologyScore(member)

				if hashtagObj.hashtag in coHashtagsMap:

					coHashtagsMap[hashtagObj.hashtag] = coHashtagsMap[hashtagObj.hashtag] + 1

				else:
					coHashtagsMap[hashtagObj.hashtag] = 1


		#sort the map by count desc
		sortedCoHashtags = sorted(coHashtagsMap.items(), key=lambda kv: kv[1], reverse=True)

		#print ('sortedCoHashtags: ' + str(sortedCoHashtags))

		for i, hashtagRow in enumerate(sortedCoHashtags):
			if i >= 50:
				break
			print('hashtagRow: ' + str(hashtagRow))
			csvData.append([ hashtagRow[0], hashtagTypeCoOccurring.name, 'NA', hashtagRow[1]])


		return csvData


















