#import models
from commons.models import User, UserProfile, SocialPlatform, EmailLog
from django.contrib.auth.models import Group
from social_django.models import UserSocialAuth
from django.db import models
from django.conf import settings
from datetime import datetime
import pytz
import random
from django.core.mail import send_mail



class UserService():

	def __init__(self, user = None):
		if user is not None:
			self.user = user
		else:
			self.user = self.getSystemUser()


	def createUser(self, username, password, **options):
		if User.objects.filter(username=username).exists():
			return User.objects.get(username=username)


		user = User(username=username, password='');
		user.set_password(password)
		group = None

		if options is not None:
			if 'firstName' in options:
				user.first_name = options.get('firstName')

			if 'lastName' in options:
				user.last_name = options.get('lastName')

			if 'email' in options:
				user.email = options.get('email')

			if 'isStaff' in options:
				user.is_staff = options.get('isStaff')

			if 'isActive' in options:
				user.is_active = options.get('isActive')

			if 'isSuperuser' in options:
				user.is_superuser = options.get('isSuperuser')

			if 'groupName' in options:
				groupName = options.get('groupName')
				group = Group.objects.filter(name=groupName).first()
		
		if group is None:
			group = Group.objects.get(name='facilitator')

		user.save()

		self.createDefaultUserProfile(user)

		#add user to user group
		group.user_set.add(user)

		group.save()

		return user

	def createDefaultUserProfile(self, user):

		sysUser = self.getSystemUser()

		return UserProfile.objects.create(user=user, created_by=sysUser.id, updated_by=sysUser.id)

	def isAdmin(self, user = None):
		u = self.user

		if user is not None:
			u = user

		if u is None:
			return False

		return len(u.groups.filter(name='admin')) > 0


	def getUser(self, id):
		return User.objects.filter(id=id).first()

	def getUserProfile(self, user=None):
		if user is None:
			user = self.user

		return UserProfile.objects.filter(user=user).first()

	def updateUserProfile(self, firstName, lastName, isAvailable, dailyAvailableStart, dailyAvailableEnd, timeZone, user=None):
		if user is None:
			user = self.user

		profile = self.getUserProfile(user)

		user.first_name = firstName
		user.last_name = lastName

		user.save()

		profile.is_available = isAvailable
		profile.daily_available_start = dailyAvailableStart
		profile.daily_available_end = dailyAvailableEnd
		profile.time_zone = timeZone

		profile.save()

		return profile

	def getUserByUsername(self, username):
		return User.objects.filter(username=username).first()


	def getNextAvailableFacilitator(self, intervention):
		nextAvailableFacilitator = None
		projectMembers = intervention.audience.project.members.all()
		socialPlatform = SocialPlatform.objects.filter(name='twitter').first()

		projectTwitterFacilitators = []

		availableFacilitators = []

		if projectMembers is not None:
			for member in projectMembers:
				user = member.user
				if len(user.social_auth.all().filter(provider='twitter')) > 0 and len(user.groups.all().filter(name='facilitator')):
					projectTwitterFacilitators.append(user)

		print("projectTwitterFacilitators len: " + str(len(projectTwitterFacilitators)))



		for facilitator in projectTwitterFacilitators:
			conversations = facilitator.conversations.all().filter(social_platform=socialPlatform).order_by('-created_date')
			profile = facilitator.profiles.all().first()
			timeZone = pytz.timezone(profile.time_zone)

			print("conversations len: " + str(len(conversations)))

			if len(conversations) < settings.MAX_OPEN_CONVERSATIONS:
				isPastWaitPeriod = False

				mostRecentConversation = conversations.first()

				if mostRecentConversation is not None:

					if (datetime.now(timeZone) - mostRecentConversation.created_date).hours >= settings.CONVERSATION_ASSIGN_WAIT_HOURS:
						isPastWaitPeriod = True

				else:
					isPastWaitPeriod = True

				if isPastWaitPeriod:
					print("isPastWaitPeriod: " + str(isPastWaitPeriod))

					if profile is not None:
						if profile.is_available:
							print("is_available: " + str(profile.is_available))
							localTime = datetime.now().astimezone(timeZone).time()
							if localTime >= profile.daily_available_start and localTime <= profile.daily_available_end:
								availableFacilitators.append(facilitator)


		if len(availableFacilitators) > 0:
			#pick one randomly
			print('availableFacilitators')
			randomSelection = random.randint(0, len(availableFacilitators) - 1)

			nextAvailableFacilitator = availableFacilitators[randomSelection]

		return nextAvailableFacilitator


	def getSystemUser(self):
		sysUser = self.getUserByUsername('svc-commons')

		if sysUser is None:
			sysUser = self.createUser('svc-commons', 'no pass')

		return sysUser


	def notifyUser(self, user, subject, message, htmlMessage=None):

		if htmlMessage is not None:
			send_mail(subject, message, settings.DEFAULT_FROM_EMAIL , [user.email], fail_silently=False, html_message=htmlMessage)
		else:
			send_mail(subject, message, settings.DEFAULT_FROM_EMAIL , [user.email], fail_silently=False, html_message=htmlMessage)


		#log in the db that the email was sent
		return EmailLog.objects.create(email_from=settings.DEFAULT_FROM_EMAIL, email_to=user.email, subject=subject, message=message, created_by=self.user.id, updated_by=self.user.id)

	def isSocialAuthUser(self, username, socialPlatformName):
		auths = UserSocialAuth.objects.filter(provider=socialPlatformName)

		if socialPlatformName == 'twitter':
			for auth in auths:
				if auth.extra_data['access_token']['screen_name'] == username:
					return True

		return False

	def getTwitterAccessToken(self, user):
		auth = user.social_auth.all().filter(provider='twitter').first()

		if auth is not None:
			return auth.extra_data['access_token']

		return None



