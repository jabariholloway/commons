from commons.models import Hashtag, HashtagType, InterventionHashtag, AudienceMemberIdeologyScore

class HashtagService():

	def __init__(self, user):
		self.user = user

	def createHashtag(self, audienceMember, hashtagTypeName, hashtag):
		hashtagType = HashtagType.objects.filter(name=hashtagTypeName).first()
		return Hashtag.objects.create(audience_member=audienceMember, 
			hashtag_type=hashtagType, hashtag=hashtag, created_by=self.user.id,
			updated_by=self.user.id)

	def getHashtags(self, audienceMember, hashtagTypeName):
		hashtagType = HashtagType.objects.filter(name=hashtagTypeName).first()
		return Hashtag.objects.filter(audience_member=audienceMember, hashtag_type=hashtagType)

	def getAudienceHashtags(self, audience, hashtagTypeName):
		hashtagObjs = []

		for member in audience.members.all():
			hashtagObjs.extend(self.getHashtags(member, hashtagTypeName))

		return hashtagObjs

	def getInterventionHashtags(self, audience):
		return InterventionHashtag.objects.filter(audience=audience)

	def createInterventionHashtag(self, audience, hashtag):
		return InterventionHashtag.objects.create(audience=audience, hashtag=hashtag,
			created_by=self.user.id, updated_by=self.user.id)

	def getAverageIdeologyScore(self, audience, hashtag, hashtagTypeName='regular'):
		hashtagType = HashtagType.objects.filter(name=hashtagTypeName).first()
		scores = []
		for member in audience.members.all():
			ideologyScore = AudienceMemberIdeologyScore.objects.filter(audience_member=member).first()
			hashtags = Hashtag.objects.filter(hashtag=hashtag, hashtag_type=hashtagType, audience_member=member)
			for tag in hashtags:
				scores.append(ideologyScore.score)

		if len(scores) > 0:
			return round(sum(scores) / len(scores), 2)

		return 0




