from commons.models import SocialPlatform, Conversation, ConversationMessage, ConversationReply

class ConversationService():

	def __init__(self, user = None):

		self.user = user


	def createConversation(self, user, socialPlatformName, intervention, link=None):
		socialPlatform = SocialPlatform.objects.filter(name=socialPlatformName).first()
		return Conversation.objects.create(social_platform=socialPlatform, user=user, intervention=intervention, link=link, created_by=self.user.id, updated_by=self.user.id)

	def createConversationMessage(self, author, conversation, socialConversationId, text, link, **options):

		conversationMessage = ConversationMessage(author=author, conversation=conversation, social_conversation_id=socialConversationId, text=text, link=link, created_by=self.user.id, updated_by=self.user.id)

		if options is not None:
			if 'repliedToUsername' in options:
				conversationMessage.replied_to_username = options.get('repliedToUsername')

			if 'repliedToSocialConversationId' in options:
				conversationMessage.replied_to_social_conversation_id = options.get('repliedToSocialConversationId')

		conversationMessage.save()
		#strip emoticons from tweet
		return conversationMessage

	def createConversationReply(self, messageRepliedTo, socialConversationId):
		return ConversationReply.objects.create(message_replied_to=messageRepliedTo, social_conversation_id=socialConversationId, created_by=self.user.id, updated_by=self.user.id)

	def getResponseNeededConversationMessages(self, conversation):
		messages = []

		for message in conversation.messages.all():
			if message.replied_to_username is not None and message.replied_to_social_conversation_id is not None and len(message.replies.all()) == 0:
				messages.append(message)

		return messages


