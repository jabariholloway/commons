from commons.forms import CustomRegistrationForm
from django.contrib.auth.models import User
from django_registration.views import RegistrationView
from commons.services.user import UserService

class RegistrationBackend(RegistrationView):

	success_url = '/users/register/success'

	def get_form_class(self):
		return CustomRegistrationForm

	def register(self, form):
		data = form.cleaned_data
		firstName = data['first_name']
		lastName = data['last_name']
		email = data['email']
		username = data['username']
		password = data['password1']

		#create user and assign user to facilitators group (default)
		userService = UserService()
		user = userService.createUser(username, password, 
			firstName=firstName, lastName=lastName, email=email)
		return user
