# Generated by Django 2.1.5 on 2019-02-18 15:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('commons', '0011_auto_20190218_1548'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='experimentgroupmemberideologyscore',
            name='experiment_group_member_id',
        ),
        migrations.AddField(
            model_name='experimentgroupmemberideologyscore',
            name='member',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='ideologyScores', to='commons.ExperimentGroupMember'),
        ),
    ]
