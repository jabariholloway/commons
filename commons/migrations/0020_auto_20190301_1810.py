# Generated by Django 2.1.5 on 2019-03-01 18:10

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('commons', '0019_auto_20190218_2141'),
    ]

    operations = [
        migrations.CreateModel(
            name='InterventionHashTag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hashtag', models.CharField(max_length=150)),
                ('created_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('created_by', models.IntegerField()),
                ('updated_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_by', models.IntegerField()),
                ('audience', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='interventionHashtags', to='commons.Audience')),
            ],
        ),
        migrations.AddField(
            model_name='intervention',
            name='audience',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='audiences', to='commons.Audience'),
        ),
        migrations.AddField(
            model_name='intervention',
            name='is_hypothesis_validated',
            field=models.BooleanField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='project',
            name='url_slog',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='audiencemember',
            name='social_id',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='intervention',
            name='content',
            field=models.CharField(blank=True, max_length=280, null=True),
        ),
    ]
