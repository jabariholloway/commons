# Generated by Django 2.1.5 on 2019-02-11 01:43

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('commons', '0003_auto_20190210_2306'),
    ]

    operations = [
        migrations.CreateModel(
            name='Audience',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('parent_id', models.IntegerField(blank=True, null=True)),
                ('name', models.CharField(max_length=100)),
                ('created_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('created_by', models.IntegerField()),
                ('updated_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_by', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='AudienceMember',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('member', models.CharField(max_length=50)),
                ('created_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('created_by', models.IntegerField()),
                ('updated_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_by', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='AudienceMemberIdeologyScore',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('score', models.IntegerField()),
                ('created_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('created_by', models.IntegerField()),
                ('updated_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_by', models.IntegerField()),
                ('audience_member_id', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='commons.AudienceMember')),
            ],
        ),
        migrations.CreateModel(
            name='AudienceType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=25)),
                ('created_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('created_by', models.IntegerField()),
                ('updated_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_by', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Conversation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('link', models.CharField(blank=True, max_length=100, null=True)),
                ('is_closed', models.CharField(blank=True, default='n', max_length=1)),
                ('close_out_time', models.DateTimeField(blank=True, null=True)),
                ('close_out_note', models.CharField(max_length=2000)),
                ('created_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('created_by', models.IntegerField()),
                ('updated_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_by', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='ConversationMessage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('author', models.CharField(max_length=50)),
                ('social_conversation_id', models.CharField(max_length=50)),
                ('text', models.CharField(max_length=500)),
                ('link', models.CharField(blank=True, max_length=100, null=True)),
                ('created_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('created_by', models.IntegerField()),
                ('updated_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_by', models.IntegerField()),
                ('conversation_id', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='commons.Conversation')),
            ],
        ),
        migrations.CreateModel(
            name='ExperimentGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('created_by', models.IntegerField()),
                ('updated_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_by', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='ExperimentGroupMember',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('created_by', models.IntegerField()),
                ('updated_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_by', models.IntegerField()),
                ('audience_member_id', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='commons.AudienceMember')),
                ('experiment_group_id', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='commons.ExperimentGroup')),
            ],
        ),
        migrations.CreateModel(
            name='ExperimentGroupMemberFollows',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('follows', models.CharField(max_length=50)),
                ('created_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('created_by', models.IntegerField()),
                ('updated_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_by', models.IntegerField()),
                ('experiment_group_member_id', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='commons.ExperimentGroupMember')),
            ],
        ),
        migrations.CreateModel(
            name='ExperimentGroupMemberIdeologyScore',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('score', models.IntegerField()),
                ('is_initial', models.CharField(blank=True, max_length=1, null=True)),
                ('created_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('created_by', models.IntegerField()),
                ('updated_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_by', models.IntegerField()),
                ('experiment_group_member_id', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='commons.ExperimentGroupMember')),
            ],
        ),
        migrations.CreateModel(
            name='ExperimentGroupType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=25)),
                ('created_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('created_by', models.IntegerField()),
                ('updated_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_by', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Hashtag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hashtag', models.CharField(max_length=150)),
                ('created_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('created_by', models.IntegerField()),
                ('updated_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_by', models.IntegerField()),
                ('audience_member_id', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='commons.AudienceMember')),
            ],
        ),
        migrations.CreateModel(
            name='HashtagType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=25)),
                ('created_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('created_by', models.IntegerField()),
                ('updated_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_by', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Intervention',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(max_length=150)),
                ('hypothesis', models.CharField(blank=True, max_length=300, null=True)),
                ('content', models.CharField(max_length=280)),
                ('start_time', models.DateTimeField()),
                ('end_time', models.DateTimeField()),
                ('created_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('created_by', models.IntegerField()),
                ('updated_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_by', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='InterventionStatus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=25)),
                ('created_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('created_by', models.IntegerField()),
                ('updated_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_by', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='SocialPlatform',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=25)),
                ('created_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('created_by', models.IntegerField()),
                ('updated_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_by', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image_path', models.CharField(blank=True, max_length=500, null=True)),
                ('is_available', models.CharField(blank=True, default='y', max_length=1)),
                ('daily_available_start', models.TimeField(blank=True, default='9:00')),
                ('daily_available_end', models.TimeField(blank=True, default='17:00')),
                ('phone', models.CharField(blank=True, max_length=10, null=True)),
                ('time_zone', models.CharField(blank=True, default='EST', max_length=3)),
                ('created_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('created_by', models.IntegerField()),
                ('updated_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_by', models.IntegerField()),
                ('user_id', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='intervention',
            name='social_platform_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='commons.SocialPlatform'),
        ),
        migrations.AddField(
            model_name='hashtag',
            name='hashtag_type_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='commons.HashtagType'),
        ),
        migrations.AddField(
            model_name='experimentgroup',
            name='group_type_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='commons.ExperimentGroupType'),
        ),
        migrations.AddField(
            model_name='experimentgroup',
            name='intervention_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='commons.Intervention'),
        ),
        migrations.AddField(
            model_name='conversation',
            name='social_platform_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='commons.SocialPlatform'),
        ),
        migrations.AddField(
            model_name='conversation',
            name='user_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='audiencemember',
            name='audience_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='commons.SocialPlatform'),
        ),
        migrations.AddField(
            model_name='audience',
            name='audienc_type_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='commons.AudienceType'),
        ),
        migrations.AddField(
            model_name='audience',
            name='social_platform_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='commons.SocialPlatform'),
        ),
    ]
