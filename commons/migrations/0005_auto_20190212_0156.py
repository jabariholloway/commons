# Generated by Django 2.1.5 on 2019-02-12 01:56

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('commons', '0004_auto_20190211_0143'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectMember',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('created_by', models.IntegerField()),
                ('updated_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_by', models.IntegerField()),
                ('access_level_id', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='commons.ProjectAccessLevel')),
                ('project_id', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='commons.Project')),
                ('user_id', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='audiencemember',
            name='follows_id',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
