$(document).ready(function () {

    $('#commons-sidebar-collapse').on('click', function () {
        $('#sidebar').toggleClass('active');
        $('#commons-project-content').toggleClass('shrink')
    });

    //set the highlighted sidebar link to whichever a tag's href
    //matches the url path

    function updateSidebarSelection() {
    	var currentPath = $(location).attr('pathname');

    	var links = $('#sidebar a');

    	$.each(links, function(i, link) {

    		link = $(link);
    		if (link.attr('href') === currentPath) {
    			link.parent().addClass('active');

    			var ul = link.closest('ul');
    			if (ul.hasClass('collapse')) {
    				ul.addClass('show');
    				var a = ul.siblings('a').first();
    				a.attr('aria-expanded', true);
    			}
    		}
    	});
    }

    updateSidebarSelection();

});