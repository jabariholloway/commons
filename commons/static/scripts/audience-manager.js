$(document).ready(function() {

	$('#commons-audience-select').change(function() {
		var val = $(this).val();

		if (val == 1) {
			//show file upload
			//d-none text area
	
			$('#commons-seed-handles-upload-container').removeClass('d-none');
			$('#commons-seed-handles-upload-container input').attr('required', 'required');
			$('#commons-base-population-text-container').addClass('d-none');
			$('#commons-base-population-text-container textarea').removeAttr('required');
			$('#commons-intervention-hashtags-text-container').addClass('d-none');
			$('#commons-intervention-hashtags-text-container textarea').removeAttr('required');
		} else if (val == 2) {
			//show text area
			//d-none file upload
			$('#commons-base-population-text-container').removeClass('d-none');
			$('#commons-base-population-text-container textarea').attr('required', 'required');
			$('#commons-intervention-hashtags-text-container').removeClass('d-none');
			$('#commons-intervention-hashtags-text-container textarea').attr('required', 'required');
			$('#commons-seed-handles-upload-container').addClass('d-none');
			$('#commons-seed-handles-upload-container input').removeAttr('required');
		}
	});

	function checkForAudienceCreation() {
		var nextStepsModal = $('#commons-next-steps-modal');

		if (nextStepsModal.hasClass('show-next-steps-modal')) {
			nextStepsModal.modal('show');
		}
	}


	$('#commons-task-test-btn').click(function() {
		console.log('clicked');
		$.ajax({
			url: '/tasks/test',
			method: 'get',
			success: function(data) {
				alert(JSON.stringify(data));
			},
			error: function(jqXhr, status, error) {
				alert(error);
			}
		})
	});

	checkForAudienceCreation()

	/*$('#commons-new-audience-twitter-form').submit(function(e) {
		e.preventDefault();
		//close mdoal
		$('.modal').modal('hide');

		var url = document.location.href + '/create';
		var formData = new FormData($(this));

		//ajax
		$.ajax({
			url: url,
			method: 'post',
			data: formData,
			success: function(data) {
				//update the view
				//show project added message
				console.log(JSON.stringify(data));

				//show next steps pop-up

			},
			error: function(jqXhr, status, error) {
				//show error message
				console.log('error: ' + error);
			}
		});
	});*/


});