$(document).ready(function() {

	//intercept 
	$('#commons-new-project-form').submit(function(e) {
		e.preventDefault();
		//close mdoal
		$('.modal').modal('hide');

		//ajax
		$.ajax({
			url: '/projects/create',
			method: 'post',
			data: $(this).serialize(),
			success: function(data) {
				//update the view
				//show project added message
				console.log(JSON.stringify(data));

			},
			error: function(jqXhr, status, error) {
				//show error message
				console.log('error: ' + error);
			}
		});
	});

	//need to add logic for requesting access to projects 
	//and for asking facilitators (non-admin) to log in with their
	//twitter account

	$('.commons-btn-join-project').click(function() {
		var btn = $(this);
		var isTwitterRequired = btn.attr('data-requires-twitter');

		//ajax
		$.ajax({
			url: '/projects/requests/create',
			method: 'post',
			data: { project_slug: btn.attr('data-project-slug'),
			csrfmiddlewaretoken: getCookie('csrftoken')
			},
			success: function(data) {
				//update the view
				//show project added message
				console.log(JSON.stringify(data));

				if (isTwitterRequired) {
					$('#twitter-required-modal').modal('show');
				}

			},
			error: function(jqXhr, status, error) {
				//show error message
				console.log('error: ' + error);
			}
		});

	});	


	function updateProjectAccessView(data) {

	}

	$('#commons-btn-login-twitter').click(function() {
		var btn = $(this);
		//create project request and then redirect to link (don't update view)
		console.log('intercepted');

		return true;
	});

	function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }


});