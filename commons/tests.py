
from django.test import TestCase
from commons.services.project import ProjectService
from commons.services.user import UserService
from commons.services.audience import AudienceService
from commons.models import ProjectAccessLevel, SocialPlatform, AudienceType
import datetime


class ProjectAccessTests(TestCase):
	created_by = 1
	updated_by = 1
	userService = None
	user1 = None
	user2 = None
	projectService = None
	start = None
	end = None
	project = None 

	def setUp(self):
		ProjectAccessLevel.objects.create(name="read only", created_by=self.created_by, updated_by=self.updated_by)
		ProjectAccessLevel.objects.create(name="edit", created_by=self.created_by, updated_by=self.updated_by)
		self.userService =  UserService()
		self.user1 = self.userService.createUser('user1','pass')
		self.user2 = self.userService.createUser('user2','pass')
		self.projectService = ProjectService(self.user1)
		self.start = datetime.datetime(2019, 2, 18)
		self.end = datetime.datetime(2019, 12, 31)
		

	def test_project_member_can_access_project(self):
		project = self.projectService.createProject('project A', 'short description', self.start, self.end)
		
		self.assertTrue(self.projectService.hasAccess(project, self.user1))

		self.assertFalse(self.projectService.hasAccess(project, self.user2))

		self.projectService.addMember(self.user2, project)

		self.assertTrue(self.projectService.hasAccess(project, self.user2))


	def test_no_duplicate_project_member_can_be_added_to_a_project(self):
		project = self.projectService.createProject('project B', 'short description', self.start, self.end)

		self.assertEqual(len(project.members.all()), 1)
		
		self.projectService.addMember(self.user1, project)

		self.assertEqual(len(project.members.all()), 1)

		self.projectService.addMember(self.user2, project)

		self.assertEqual(len(project.members.all()), 2)


class AudienceBuilderTests(TestCase):
	created_by = 1
	updated_by = 1
	userService = None
	user = None
	audienceService = None

	def setUp(self):
		SocialPlatform.objects.create(name="twitter", created_by=self.created_by, updated_by=self.updated_by)
		SocialPlatform.objects.create(name="facebook", created_by=self.created_by, updated_by=self.updated_by)
		AudienceType.objects.create(name="seed", created_by=self.created_by, updated_by=self.updated_by)
		AudienceType.objects.create(name="base population", created_by=self.created_by, updated_by=self.updated_by)
		self.userService =  UserService()
		self.user = self.userService.createUser('user1','pass')
		self.audienceService = AudienceService(self.user)


	def test_create_audience_from_csv(self):

		testCsvFile1 = open('commons/test_files/handles_test.csv')

		audience = self.audienceService.createAudienceFromCsv(testCsvFile1)

		self.assertEqual(audience.name, 'handles_test');

		member1QS = audience.members.filter(username='lutherstrange')

		self.assertEqual(len(member1QS), 1)

		member2QS = audience.members.filter(username='SenShelby')

		self.assertEqual(len(member1QS), 1)

		member1 = member1QS.first()

		self.assertEqual(member1.ideologyScores.all().first().score, -0.5)

		member2 = member2QS.first()

		self.assertEqual(member2.ideologyScores.all().first().score, 0.5)

		testCsvFile1.close()


		testCsvFile1 = open('commons/test_files/handles_test.csv')

		audience = self.audienceService.createAudienceFromCsv(testCsvFile1, audienceName='test audience')

		self.assertEqual(audience.name, 'test audience');

		testCsvFile1.close()

		try:
			testCsvFile2 = open('commons/test_files/handles_test_2.csv')
			audience = self.audienceService.createAudienceFromCsv(testCsvFile2)
			self.fail("seed audience csv file without headers Username and Score must throw value error")
		except ValueError:
			pass

		try:
			testCsvFile3 = open('commons/test_files/handles_test_3.csv')
			audience = self.audienceService.createAudienceFromCsv(testCsvFile3)
			self.fail("seed audience csv file with proper headers and no data must throw value error")
		except ValueError:
			pass










#class InterventionTests(TestCase):
