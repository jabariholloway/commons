from django.db import models
from datetime import datetime
from django.contrib.auth.models import User
from social_django.models import UserSocialAuth
from django.utils import timezone

class UserProfile(models.Model):
	user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='profiles')
	image_path = models.CharField(max_length=500, null=True, blank=True)
	is_available = models.BooleanField(default=True, blank=True)
	daily_available_start = models.TimeField(blank=True, default='9:00')
	daily_available_end = models.TimeField(blank=True, default='17:00')
	phone = models.CharField(max_length=10, blank=True, null=True)
	time_zone = models.CharField(max_length=50, blank=True, default='US/Eastern')
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class SocialAuthLink(models.Model):
	user = models.ForeignKey(User, on_delete=models.PROTECT)
	social_auth = models.ForeignKey(UserSocialAuth, on_delete=models.PROTECT)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()



class Project(models.Model):
	name = models.CharField(max_length=100)
	url_slug = models.CharField(max_length=100, blank=True, null=True)
	short_description = models.CharField(max_length=50)
	long_description = models.CharField(max_length=150, default=None, blank=True, null=True)
	start_date = models.DateTimeField()
	end_date = models.DateTimeField()
	is_admin_only = models.BooleanField(default=False, blank=True)
	is_twitter_required = models.BooleanField(default=False, blank=True)
	is_private = models.BooleanField(default=False, blank=True)
	project_owner = models.ForeignKey(User, on_delete=models.PROTECT, related_name='owner', null=True)
	is_deleted = models.BooleanField(default=False, blank=True)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class ProjectAccessLevel(models.Model):
	name = models.CharField(max_length=25)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class ProjectRequestStatus(models.Model):
	name = models.CharField(max_length=25)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class ProjectAccessRequest(models.Model):
	user = models.ForeignKey(User, on_delete=models.PROTECT)
	project = models.ForeignKey(Project, on_delete=models.PROTECT)
	access_level = models.ForeignKey(ProjectAccessLevel, on_delete=models.PROTECT, default=2)
	request_status = models.ForeignKey(ProjectRequestStatus, on_delete=models.PROTECT)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class ProjectAccessRequestLog(models.Model):
	user = models.ForeignKey(User, on_delete=models.PROTECT)
	project = models.ForeignKey(Project, on_delete=models.PROTECT)
	access_level = models.ForeignKey(ProjectAccessLevel, on_delete=models.PROTECT, default=2)
	request_status = models.ForeignKey(ProjectRequestStatus, on_delete=models.PROTECT)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class ProjectMember(models.Model):
	project = models.ForeignKey(Project, on_delete=models.PROTECT, related_name='members', null=True)
	user = models.ForeignKey(User, on_delete=models.PROTECT)
	access_level = models.ForeignKey(ProjectAccessLevel, on_delete=models.PROTECT)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class SocialPlatform(models.Model):
	name = models.CharField(max_length=25)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class AudienceType(models.Model):
	name = models.CharField(max_length=25)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class ProcessStatus(models.Model):
	name = models.CharField(max_length=25)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class Audience(models.Model):
	parent_id = models.IntegerField(blank=True, null=True)
	project = models.ForeignKey(Project, on_delete=models.PROTECT, related_name='audiences', null=True)
	social_platform = models.ForeignKey(SocialPlatform, on_delete=models.PROTECT, related_name='socialPlatform', null=True)
	audience_type = models.ForeignKey(AudienceType, on_delete=models.PROTECT, related_name='audienceType', null=True)
	name = models.CharField(max_length=100)
	description = models.CharField(max_length=100, null=True)
	status = models.ForeignKey(ProcessStatus, on_delete=models.PROTECT, related_name='audienceStatus', null=True)
	is_deleted = models.BooleanField(default=False, blank=True)
	is_seed_hashtags_ready = models.BooleanField(default=False, blank=True)
	is_co_hashtags_ready = models.BooleanField(default=False, blank=True)
	is_base_population_ready = models.BooleanField(default=False, blank=True)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class AudienceMember(models.Model):
	audience = models.ForeignKey(Audience, on_delete=models.PROTECT, null=True, related_name='members')
	username = models.CharField(max_length=50, blank=True, null=True)
	social_id = models.CharField(max_length=50, blank=True, null=True)
	follows_id = models.IntegerField(blank=True, null=True)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class AudienceMemberIdeologyScore(models.Model):
	audience_member = models.ForeignKey(AudienceMember, on_delete=models.PROTECT, related_name='ideologyScores')
	score = models.FloatField()
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class InterventionHashtag(models.Model):
	audience = models.ForeignKey(Audience, on_delete=models.PROTECT, null=True, related_name='interventionHashtags')
	hashtag = models.CharField(max_length=150)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()


class HashtagType(models.Model):
	name = models.CharField(max_length=25)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class Hashtag(models.Model):
	audience_member = models.ForeignKey(AudienceMember, on_delete=models.PROTECT, related_name='hashtags')
	hashtag_type = models.ForeignKey(HashtagType, on_delete=models.PROTECT)
	hashtag = models.CharField(max_length=150)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class InterventionStatus(models.Model):
	name = models.CharField(max_length=25)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class Intervention(models.Model):
	project = models.ForeignKey(Project, on_delete=models.PROTECT, related_name='project_interventions', null=True)
	social_platform = models.ForeignKey(SocialPlatform, on_delete=models.PROTECT)
	audience = models.ForeignKey(Audience, on_delete=models.PROTECT, related_name='interventions', null=True)
	description = models.CharField(max_length=150)
	hypothesis = models.CharField(max_length=300, blank=True, null=True)
	is_hypothesis_validated = models.BooleanField(blank=True, null=True)
	is_deleted = models.BooleanField(default=False, blank=True)
	start_time = models.DateTimeField()
	end_time = models.DateTimeField()
	status = models.ForeignKey(InterventionStatus, on_delete=models.PROTECT, null=True)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

#survey input, definitions / general experience in political climate, hashtags
class InterventionContentGroupType(models.Model):
	name = models.CharField(max_length=25)
	display_name = models.CharField(max_length=50)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()


class InterventionContentGroup(models.Model):
	intervention = models.ForeignKey(Intervention, on_delete=models.PROTECT, related_name="contentGroups")
	group_type = models.ForeignKey(InterventionContentGroupType, on_delete=models.PROTECT, related_name='groupType', null=True)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class InterventionMessageGroup(models.Model):
	content_group = models.ForeignKey(InterventionContentGroup, on_delete=models.PROTECT, related_name="messageGroups")
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class InterventionMessageGroupHashtag(models.Model):
	message_group = models.ForeignKey(InterventionMessageGroup, on_delete=models.PROTECT, related_name="hashtags", null=True)
	hashtag = models.CharField(max_length=150)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()


class InterventionMessage(models.Model):
	message_group = models.ForeignKey(InterventionMessageGroup, on_delete=models.PROTECT, related_name="messages", null=True)
	text = models.CharField(max_length=280)
	image_path = models.CharField(max_length=400, blank=True, null=True)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class InterventionRule(models.Model):
	social_platform = models.ForeignKey(SocialPlatform, on_delete=models.PROTECT)
	name = models.CharField(max_length=250)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class ExperimentGroupType(models.Model):
	name = models.CharField(max_length=25)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class ExperimentGroup(models.Model):
	intervention = models.ForeignKey(Intervention, on_delete=models.PROTECT, related_name="experimentGroups")
	group_type = models.ForeignKey(ExperimentGroupType, on_delete=models.PROTECT)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class ExperimentGroupMember(models.Model):
	experiment_group = models.ForeignKey(ExperimentGroup, on_delete=models.PROTECT, related_name="members")
	audience_member = models.ForeignKey(AudienceMember, on_delete=models.PROTECT)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class ExperimentGroupMemberFollows(models.Model):
	member = models.ForeignKey(ExperimentGroupMember, on_delete=models.PROTECT, related_name='follows', null=True)
	follows_id = models.CharField(max_length=25, null=True)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class ExperimentGroupMemberIdeologyScore(models.Model):
	member = models.ForeignKey(ExperimentGroupMember, on_delete=models.PROTECT, related_name='ideologyScores', null=True)
	score = models.IntegerField()
	is_initial = models.BooleanField(null=True, blank=True)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()


class ConversationActionType(models.Model):
	name = models.CharField(max_length=25)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()


class Conversation(models.Model):
	social_platform = models.ForeignKey(SocialPlatform, on_delete=models.PROTECT)
	intervention = models.ForeignKey(Intervention, on_delete=models.PROTECT, related_name="conversations", null=True)
	user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='conversations')
	link = models.CharField(max_length=100, blank=True, null=True)
	is_closed = models.BooleanField(default=False, blank=True)
	close_out_time = models.DateTimeField(null=True, blank=True)
	close_out_note = models.CharField(max_length=2000)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class ConversationAction(models.Model):
	conversation = models.ForeignKey(Conversation, on_delete=models.PROTECT)
	action_type = models.ForeignKey(ConversationActionType, on_delete=models.PROTECT)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()


class ConversationMessage(models.Model):
	conversation = models.ForeignKey(Conversation, on_delete=models.PROTECT, related_name='messages')
	author = models.CharField(max_length=100)
	replied_to_username = models.CharField(max_length=100, blank=True, null=True)
	replied_to_social_conversation_id = models.CharField(max_length=100, blank=True, null=True)
	social_conversation_id = models.CharField(max_length=50)
	text = models.CharField(max_length=500)
	link = models.CharField(max_length=100, blank=True, null=True)
	social_created_at = models.DateTimeField(null=True, blank=True)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class ConversationReply(models.Model):
	message_replied_to = models.ForeignKey(ConversationMessage, on_delete=models.PROTECT, related_name='replies')
	social_conversation_id = models.CharField(max_length=50) 
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class ApiAccount(models.Model):
	social_platform = models.ForeignKey(SocialPlatform, on_delete=models.PROTECT)
	name = models.CharField(max_length=50)
	consumer_key = models.CharField(max_length=100, null=True, blank=True)
	consumer_secret = models.CharField(max_length=100, null=True, blank=True)
	access_token = models.CharField(max_length=100, null=True, blank=True)
	access_token_secret = models.CharField(max_length=100, null=True, blank=True)
	is_reserved =  models.BooleanField(default=False, blank=True)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()

class EmailLog(models.Model):
	email_from = models.CharField(max_length=100)
	email_to = models.CharField(max_length=250)
	subject = models.CharField(max_length=150)
	message = models.CharField(max_length=2000)
	created_date = models.DateTimeField(default=timezone.now, blank=True)
	created_by = models.IntegerField()
	updated_date = models.DateTimeField(default=timezone.now, blank=True)
	updated_by = models.IntegerField()


