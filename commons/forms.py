from django_registration.forms import RegistrationForm
from django.contrib.auth.models import User
from django import forms

class CustomRegistrationForm(RegistrationForm):
	first_name = forms.CharField(label='First name', max_length=30,required=True)
	last_name = forms.CharField(label='Last name', max_length=30, required=True)

	class Meta:
		model = User
		fields = ('first_name', 'last_name', 'email', 'username', 'password1', 'password2')