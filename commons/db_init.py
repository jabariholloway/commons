#add initial values to the db

from commons.models import ProjectAccessLevel, ProjectRequestStatus, SocialPlatform, AudienceType, HashtagType, InterventionStatus, ExperimentGroupType, ApiAccount, ProcessStatus
from django.contrib.auth.models import Group

created_by = 1
updated_by = 1

#api accounts
twitter = SocialPlatform.objects.get(name='twitter')
ApiAccount.objects.create(social_platform=twitter, name='WillSmith23', consumer_key='YfxxfFTSDyTB4UUserzECWFth',
	consumer_secret='FUdqtD4XYS0rBGbJhO20aSWcJTYhdiEsRRZx3QallHIOXQ3E1M',
	access_token='892418670755971072-kHfu0z6g3a1zOat8WnDf3QVPn3Sxk2U',
	access_token_secret='FJ1n3L9aaadq4PfcMY5riTpgf6xAjNxZs2vpGSMGtmjPe',
	created_by=1, updated_by=1)

ApiAccount.objects.create(social_platform=twitter, name='the_commons', consumer_key='rn6b2Yopy403yOKLP9Rj6MkTG',
	consumer_secret='Q6fOs17Rnf5cHdRm9ZuaiJJ8jsb2nBqC8JhP5C5jgDaHtls5RK',
	access_token='900021286117806080-2g4wnsg5kxPf9YhaemiKLmrWWeXn6RB',
	access_token_secret='STrIVrlrLf3BqTOBdgbhrAXcHNSGPmPDl1tOtbCp0OA3N',
	created_by=1, updated_by=1, is_reserved=True)

ApiAccount.objects.create(social_platform=twitter, name='SamsonHand', consumer_key='nnpmb1yLmPzIBbwnwEm9nHdBm',
	consumer_secret='QVnJvnzDH5sk19rfr18dnm8y0wWBaGbnv84A8No9y3MRKpgwy2',
	access_token='892526993807478785-SLpmY4rj1mjXR79zQhkE1Qdcz6Ys2S7',
	access_token_secret='XvQ2RoTqlPZhivwS7qM3QBSubIzK7cmwVYfhRpv2PZXF8',
	created_by=1, updated_by=1)

ApiAccount.objects.create(social_platform=twitter, name='smellycakes', consumer_key='uNrztx4NIvZNLnvryYsiprXyD',
	consumer_secret='mxJXRYFPyi69dWJlWiIufqynZNpimo4ajAVCdEeGLf0Xso8Uxp',
	access_token='899968996874825728-VrHRqzMmB54Y2cQtGaqESCz6cUvXBDb',
	access_token_secret='NeHf5ZHxKLnhvmjYFS5x9vpn0kFg4nQS08iqAVKbfMgv7',
	created_by=1, updated_by=1)

ApiAccount.objects.create(social_platform=twitter, name='photosplash', consumer_key='mLgNNSP9z2PurK0dHMC6KjTFR',
	consumer_secret='L2ULFkdaj4aXEJUCv2uIoWOc368rwEgNtjDadn1jTCOiNdp2Zd',
	access_token='899980439632306176-hpJV1eGR1DpZ1fIJTceOw2ODIQaBMIS',
	access_token_secret='RwjwLxgpkI5ZEoKDJXeshGFLkR3uUSitl5JX5CZgbF7VU',
	created_by=1, updated_by=1)




#lookups

#ProjectAccessLevel.objects.create(name="read only", created_by=created_by, updated_by=updated_by)
ProjectAccessLevel.objects.create(name="edit", created_by=created_by, updated_by=updated_by)

ProjectRequestStatus.objects.create(name="requested", created_by=created_by, updated_by=updated_by)
ProjectRequestStatus.objects.create(name="approved", created_by=created_by, updated_by=updated_by)
ProjectRequestStatus.objects.create(name="denied", created_by=created_by, updated_by=updated_by)
ProjectRequestStatus.objects.create(name="canceled", created_by=created_by, updated_by=updated_by)

SocialPlatform.objects.create(name="twitter", created_by=created_by, updated_by=updated_by)
SocialPlatform.objects.create(name="facebook", created_by=created_by, updated_by=updated_by)

AudienceType.objects.create(name="seed", created_by=created_by, updated_by=updated_by)
AudienceType.objects.create(name="base population", created_by=created_by, updated_by=updated_by)

ProcessStatus.objects.create(name="processing", created_by=created_by, updated_by=updated_by)
ProcessStatus.objects.create(name="ready", created_by=created_by, updated_by=updated_by)
ProcessStatus.objects.create(name="error", created_by=created_by, updated_by=updated_by)

HashtagType.objects.create(name="regular", created_by=created_by, updated_by=updated_by)
HashtagType.objects.create(name="co-occuring", created_by=created_by, updated_by=updated_by)
HashtagType.objects.create(name="intervention", created_by=created_by, updated_by=updated_by)

InterventionStatus.objects.create(name="scheduled", created_by=created_by, updated_by=updated_by)
InterventionStatus.objects.create(name="in progress", created_by=created_by, updated_by=updated_by)
InterventionStatus.objects.create(name="paused", created_by=created_by, updated_by=updated_by)
InterventionStatus.objects.create(name="stopped", created_by=created_by, updated_by=updated_by)
InterventionStatus.objects.create(name="completed", created_by=created_by, updated_by=updated_by)

ExperimentGroupType.objects.create(name="treatment", created_by=created_by, updated_by=updated_by)
ExperimentGroupType.objects.create(name="control", created_by=created_by, updated_by=updated_by)

Group.objects.create(name="admin")
Group.objects.create(name="facilitator")


#users