from __future__ import unicode_literals
import tweepy
from django.conf import settings
from datetime import datetime, timedelta
import time
import logging

logger = logging.getLogger(__name__)



#handles all twitter operations for the commons project
#when a twitter job is scheduled, it will use the class below
#(and relevant services) to complete the job

#remember that each job must run one at a time and therefore be part
#of a job queue

#when a user builds a twitter audience,
#a job is added to the db that happens to be scheduled
#to run immediately (as soon as all other jobs are finished)

# a job should run in a separate process and not block the site
# so it requires another project/program that listeners for jobs,
# runs one job at a time (updates the job status to in-progress),
# and updates the status of the job after it's done running (complete, failed, etc.)

#need some way of accessing django settings for Twitter even though django
#may not be available to the job runner

#functions for twitter

#getListOfFollowerIds(username)

#getTimeline(username) (actually this is getHashtagsUsed(username))

#getCoOccuringHashtags(hashtags)

#streamTweets(hashtags)

#postTweet(tweetContent)

#receiveTweetResponse?



#functions for audience service

#createBasePopulation

class TweeAccount():

	followers_queue = []

	def __init__(self, accessToken = None, accessTokenSecret = None, consumerKey = None, consumerSecret = None):

		if consumerKey is not None:
			self.consumerKey = consumerKey
		else:
			self.consumerKey = settings.SOCIAL_AUTH_TWITTER_KEY

		if consumerSecret is not None:
			self.consumerSecret = consumerSecret
		else:
			self.consumerSecret = settings.SOCIAL_AUTH_TWITTER_SECRET
		

		if accessToken is not None:
			self.accessToken = accessToken
		else:
			self.accessToken = settings.SOCIAL_AUTH_TWITTER_ACCESS_TOKEN

		if accessTokenSecret is not None:
			self.accessTokenSecret = accessTokenSecret
		else:
			self.accessTokenSecret = settings.SOCIAL_AUTH_TWITTER_ACCESS_TOKEN_SECRET

		auth = tweepy.OAuthHandler(self.consumerKey, self.consumerSecret)
		auth.set_access_token(self.accessToken, self.accessTokenSecret)

		self.api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True,
			compression=True)


	def getFollowerIds(self, username):

		ids = []

		for page in tweepy.Cursor(self.api.followers_ids, id=username).pages():
			ids.extend(page)

		return ids

	def getFriendsIds(self, username):
		friendsIds = []
		#users = []
		for friend_id in tweepy.Cursor(self.api.friends_ids, id=username).items():
			friendsIds.append(friend_id)
			#users.extend(user)

		#print(users[0].id_str)

		#for user in users:
		#	friendsIds.append(user.id_str)
		return friendsIds
		#return users

	def getFollowersQueue(self):
		return self.followers_queue


	def getUserTimeline(self, username, **options):

		statuses = []

		end_date = None
		past_num_days = None
		extra_options = {}

		if options is not None:

			if 'endDate' in options:
				end_date = options.get('endDate')

			if 'pastNumDays' in options:
				past_num_days = options.get('pastNumDays')

			if 'sinceId' in options:
				extra_options['since_id'] = options.get('sinceId')


		if past_num_days is not None:
			end_date = datetime.now() - timedelta(days=past_num_days)

		
		for status in tweepy.Cursor(self.api.user_timeline, screen_name=username, full_text=True, tweet_mode='extended', **extra_options).items():
			if (end_date is not None):
				if status.created_at < end_date:
					break

			statuses.append(status)

		return statuses


	def getTweetReplies(self, inReplyToStatusId, username, sinceStatusId):

		replies = []

		timeline = self.getUserTimeline(username, sinceId=sinceStatusId)

		for status in timeline:
			if hasattr(status, 'in_reply_to_status_id'):
				if status.in_reply_to_status_id_str == inReplyToStatusId:
					replies.append(status)

		return replies





	def getTweetedHashtags(self, username, **options):

		hashtags = []

		for status in self.getUserTimeline(username, **options):
			hashtags.extend(self.getHashTagsFromStatus(status))

		return hashtags


	def getHashTagsFromStatus(self, status):
		hashtags = []

		if hasattr(status, "entities"):
			entities = status.entities
			if "hashtags" in entities:
				for ent in entities["hashtags"]:
					if ent is not None:
						if "text" in ent:
							hashtag = ent["text"]
							if hashtag is not None:
								#print(hashtag)
								hashtags.append(hashtag)
		return hashtags



	def getCoOccuringHashtags(self, hashtagsSet):

		coOccuringHashtags = []

		query = ""

		for tag in hashtagsSet:
			formattedTag = tag
			if "#" not in formattedTag:
				formattedTag = "#" + formattedTag

			if len(query + " OR " + formattedTag) > 500:
				break

			if len(query) == 0:
				query = query + formattedTag
			else:
				query = query + " OR " + formattedTag

		logger.info("query length: " + str(len(query)))

		if len(query) > 0:
			for status in tweepy.Cursor(self.api.search,q=query, count=100, include_entities=True, lang="en").items():
				for hashtag in self.getHashTagsFromStatus(status):
					if hashtag not in query:
						coOccuringHashtags.append(hashtag)
				if len(coOccuringHashtags) > 100:
					break

		return coOccuringHashtags



		#options has: image and inReplyToTweetId
		#username must be included in tweet if inReplyToTweetId is used
		#use reply endpoint for replying to tweets
		#because it's cleaner and then we don't have
		#to use @ directly in the tweet 
	def tweet(self, message, **options):
		status = None
		if options is not None:
			if 'inReplyToTweetId' in options and 'username' in options:
				status = self.api.update_status('@' + options.get('username') + ' ' + message, options.get('inReplyToTweetId'))

		if status is None:
			status = self.api.update_status(message)
		#need a way to confirm this works (programmatically, each time) (check status props)
		return status

	def getTweetUrl(self, statusObj):
		return 'https://twitter.com/' + statusObj.user.screen_name + '/status/' + statusObj.id_str

	def getFullText(self, statusObj):
		if hasattr(statusObj, 'full_text'):
			return statusObj.full_text
			
		text = statusObj.text
		if hasattr(statusObj, 'extended_tweet'):
			text = statusObj.extended_tweet['full_text']

		return text

	def containsHashtags(self, statusObj, hashtags):
		statusHashtags = self.getHashTagsFromStatus(statusObj)

		for hashtag in statusHashtags:
			if hashtag.replace('#','') in hashtags:
				return True

		return False

	def getFirstMatchingHashtag(self, statusObj, hashtags):
		statusHashtags = self.getHashTagsFromStatus(statusObj)

		for hashtag in statusHashtags:
			tag = hashtag.replace('#','')
			if tag in hashtags:
				return tag

		return ''


#	def isLead(twitterId, basePopulation):


#	def isPolarized(twitterId):

	def getUser(self, username):
		return self.api.get_user(username)

	def streamTweets(self,  hashtags, statusQueue):
		#just get hashtags from intervention's audience
		stream = TweeStream(self, statusQueue).getStream()
		stream.filter(track=hashtags, is_async=True)
		return stream



class TweeStream(tweepy.StreamListener):

	def __init__(self, tweeAccount, statusQueue):
		super(TweeStream, self).__init__()
		self.tweeAccount = tweeAccount
		self.statusQueue = statusQueue
		self.keepRunning = True

	def on_status(self, status):
		#push the status to a background task so that on_status doesn't get blocked
		#stream will be started with filter that only contains intervention hashtags
		#check if tweet author is in base population and is not part of the team
		#check other intervention rules
		#if candidate
		#get next available facilitator
		#create new tweeAccount from their UserSocialAuth
		#tweet intervention message in reply to this tweet (start conversation)
		#notify facilitator 
		#move tweet author into treatment group and get author's followers
		#if no facilitator available move author into control group


		#need to see if Tweepy handles rate limiting for tweestream

		#wait 1min
		#then wait 5min
		#then wait 15min, and exponentially thereafter
		#check if we can still make GET requests while twitter stream is open
		if (not status.retweeted) and ('RT @' not in status.text):
			print(status.user.screen_name)
			print(self.tweeAccount.getFullText(status))
			self.statusQueue.put(status)

		return self.keepRunning


	def on_error(self, status_code):
		if status_code == 420:
			#returning False in on_data disconnects the stream
			#sleep for 15min
			time.sleep(900)
			return True


	def stop(self):
		self.keepRunning = False

	def getStream(self):
		return tweepy.Stream(auth=self.tweeAccount.api.auth, listener=self, tweet_mode='extended')







