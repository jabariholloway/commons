#need to be able to manually assign the conversation posts to facilitators
#this means that there will be interventions/posts that occur that are not
#"owned" by anyone or assigned to anyone by default. actually they are 
#owned by the Commons FB account. so admin need to know about these
#posts and be able to assign facilitators directly to them.
#these posts still count as conversations so the conversation service just 
#needs to be updated to have a manual assignment option ()

#for facebook, the interventions(campaigns) will be started via facebook
#so a background task must poll the Commons fb page to see what new
#campaigns exists and load the information for these into the system

#then, admins can view these interventions(campaigns) and assign facilitators
#to these interventions(campaigns) which kicks off a conversation (the conversation
#will have the ad creative message as its content)

#and we need to do this because of our offline events (workshops) (the targeting needs to match
#where the facilitators live)

#michaela is going to join as a facilitator as the commons account and needs 10 conversations instead of 40
#we can make a commons facilitator account 

#remember that num_conversations_limit_per_week = 40

#for the commons facilitator account the limit will be 10 conversations per week

#jacob and i will handle the creation of the Commons account for the website

#list of holes:
#1. connecting the fb account (access tokens)
#2. registering the commons twitter account (we will test tweeting at jacob's twitter account with the commons account)
#3. do we use a separate set of keys for the streaming api?
#4. need to check if followers are bots (if so, they must be eliminated) 
#5. let jacob know of any holes that come up so he can fill them